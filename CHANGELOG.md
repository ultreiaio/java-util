# Java Util changelog

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2024-05-21 12:15.

## Version [1.2.1](https://gitlab.com/ultreiaio/java-util/-/milestones/39)

**Closed at 2024-05-21.**


### Issues
  * [[enhancement 56]](https://gitlab.com/ultreiaio/java-util/-/issues/56) **Add equals and hascode methods on TwoSideContext and LeftOrRightContext** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 57]](https://gitlab.com/ultreiaio/java-util/-/issues/57) **Improve LeftOrRightContextAdapter and TwoSideContextAdapter to works with last version of gson** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.2.0](https://gitlab.com/ultreiaio/java-util/-/milestones/38)
TwoSide API

**Closed at 2024-04-22.**


### Issues
  * [[enhancement 53]](https://gitlab.com/ultreiaio/java-util/-/issues/53) **add gson adapters for TwoSideContext** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 54]](https://gitlab.com/ultreiaio/java-util/-/issues/54) **rename TwoSide.consume method to accept** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 55]](https://gitlab.com/ultreiaio/java-util/-/issues/55) **rename TwoSideContext.then method and add new apply method** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.16](https://gitlab.com/ultreiaio/java-util/-/milestones/37)

**Closed at 2024-04-22.**


### Issues
  * [[enhancement 52]](https://gitlab.com/ultreiaio/java-util/-/issues/52) **Introduce LeftOrRight (as a TwoSide) and LeftOrRightContext** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.15](https://gitlab.com/ultreiaio/java-util/-/milestones/36)

**Closed at 2024-03-29.**


### Issues
  * [[enhancement 51]](https://gitlab.com/ultreiaio/java-util/-/issues/51) **Add a simple implementation of TwoSideContext and a static factory method** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.14](https://gitlab.com/ultreiaio/java-util/-/milestones/35)

**Closed at 2024-03-20.**


### Issues
  * [[bug 50]](https://gitlab.com/ultreiaio/java-util/-/issues/50) **Fix io.ultreia.java4all.util.TwoSideContext.then method (results are flipped :D)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.13](https://gitlab.com/ultreiaio/java-util/-/milestones/34)

**Closed at 2024-03-19.**


### Issues
  * [[enhancement 49]](https://gitlab.com/ultreiaio/java-util/-/issues/49) **Introduce TwoSide and TwoSideContext (moving them from java-lang project)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.12](https://gitlab.com/ultreiaio/java-util/-/milestones/33)

**Closed at 2024-03-04.**


### Issues
  * [[enhancement 47]](https://gitlab.com/ultreiaio/java-util/-/issues/47) **Add method SingletonSupplier.toOptional** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 48]](https://gitlab.com/ultreiaio/java-util/-/issues/48) **Add more methods to deal with maps in JsonHelper** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.11](https://gitlab.com/ultreiaio/java-util/-/milestones/32)

**Closed at 2023-12-01.**


### Issues
  * [[bug 46]](https://gitlab.com/ultreiaio/java-util/-/issues/46) **Make DataMatrix.cellValueFormatter transient (since this object can be used in json flow)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.10](https://gitlab.com/ultreiaio/java-util/-/milestones/31)

**Closed at 2023-11-28.**


### Issues
  * [[bug 45]](https://gitlab.com/ultreiaio/java-util/-/issues/45) **Fix format cell method :(** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.9](https://gitlab.com/ultreiaio/java-util/-/milestones/30)

**Closed at 2023-11-28.**


### Issues
  * [[enhancement 44]](https://gitlab.com/ultreiaio/java-util/-/issues/44) **Add new cellFormatter in DataMatrix to add nice serialization on objects** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.8](https://gitlab.com/ultreiaio/java-util/-/milestones/29)

**Closed at 2023-05-31.**


### Issues
  * [[enhancement 43]](https://gitlab.com/ultreiaio/java-util/-/issues/43) **Add a new method SqlScript.copyAndCompress** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.7](https://gitlab.com/ultreiaio/java-util/-/milestones/28)

**Closed at 2022-12-19.**


### Issues
  * [[bug 41]](https://gitlab.com/ultreiaio/java-util/-/issues/41) **Revert DateParser fix (not working!)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 42]](https://gitlab.com/ultreiaio/java-util/-/issues/42) **Introduce Matrix package** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.6](https://gitlab.com/ultreiaio/java-util/-/milestones/27)

**Closed at 2022-10-24.**


### Issues
  * [[bug 40]](https://gitlab.com/ultreiaio/java-util/-/issues/40) **Fix DateAdapter (time zone are not well managed)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.5](https://gitlab.com/ultreiaio/java-util/-/milestones/26)

**Closed at 2022-09-30.**


### Issues
  * [[enhancement 39]](https://gitlab.com/ultreiaio/java-util/-/issues/39) **Add commit option on SqlConsumer (since coming from a JdbcHelper we need to be able to commit stuff!)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.4](https://gitlab.com/ultreiaio/java-util/-/milestones/25)

**Closed at 2022-08-25.**


### Issues
  * [[enhancement 38]](https://gitlab.com/ultreiaio/java-util/-/issues/38) **Introduce SqlSupplier** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.3](https://gitlab.com/ultreiaio/java-util/-/milestones/24)

**Closed at 2022-08-22.**


### Issues
  * [[enhancement 37]](https://gitlab.com/ultreiaio/java-util/-/issues/37) **Introduce JdbcConfiguration (moved from Topia project)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.2](https://gitlab.com/ultreiaio/java-util/-/milestones/23)

**Closed at 2022-07-31.**


### Issues
No issue.

## Version [1.1.1](https://gitlab.com/ultreiaio/java-util/-/milestones/22)

**Closed at 2022-07-31.**


### Issues
  * [[enhancement 32]](https://gitlab.com/ultreiaio/java-util/-/issues/32) **Improve TimeLog** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 33]](https://gitlab.com/ultreiaio/java-util/-/issues/33) **Add SqlQuery and SqlFunction in sql package** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 34]](https://gitlab.com/ultreiaio/java-util/-/issues/34) **Remove CallAnalyse class** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 35]](https://gitlab.com/ultreiaio/java-util/-/issues/35) **Remove guava json adapters from this project (remove then guava dependency :))** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 36]](https://gitlab.com/ultreiaio/java-util/-/issues/36) **Make TimeLog gives us the current location of call :D** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.0](https://gitlab.com/ultreiaio/java-util/-/milestones/21)

**Closed at 2022-07-30.**


### Issues
  * [[bug 30]](https://gitlab.com/ultreiaio/java-util/-/issues/30) **Introduce json package (from http project)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 30]](https://gitlab.com/ultreiaio/java-util/-/issues/30) **Introduce json package (from http project)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 31]](https://gitlab.com/ultreiaio/java-util/-/issues/31) **Introduce sql package (from topia project)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.19](https://gitlab.com/ultreiaio/java-util/-/milestones/20)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.18](https://gitlab.com/ultreiaio/java-util/-/milestones/19)

**Closed at 2021-12-10.**


### Issues
  * [[bug 29]](https://gitlab.com/ultreiaio/java-util/-/issues/29) **Open mode RecursivePropertyResolver API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.17](https://gitlab.com/ultreiaio/java-util/-/milestones/18)

**Closed at 2021-12-10.**


### Issues
  * [[bug 28]](https://gitlab.com/ultreiaio/java-util/-/issues/28) **Fix RecursivePropertiesResolver** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.16](https://gitlab.com/ultreiaio/java-util/-/milestones/17)

**Closed at 2021-12-10.**


### Issues
  * [[enhancement 26]](https://gitlab.com/ultreiaio/java-util/-/issues/26) **Introduce RecursivePropertiesResolver** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 27]](https://gitlab.com/ultreiaio/java-util/-/issues/27) **Introduce simple io API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.15](https://gitlab.com/ultreiaio/java-util/-/milestones/16)

**Closed at 2021-12-06.**


### Issues
  * [[enhancement 25]](https://gitlab.com/ultreiaio/java-util/-/issues/25) **Improve RecursiveProperties** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.14](https://gitlab.com/ultreiaio/java-util/-/milestones/15)

**Closed at 2021-11-13.**


### Issues
  * [[enhancement 24]](https://gitlab.com/ultreiaio/java-util/-/issues/24) **Introduce Version (get it back from nuiton)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.13](https://gitlab.com/ultreiaio/java-util/-/milestones/14)

**Closed at 2021-06-10.**


### Issues
  * [[enhancement 23]](https://gitlab.com/ultreiaio/java-util/-/issues/23) **Add method Zips.compressFiles** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.12](https://gitlab.com/ultreiaio/java-util/-/milestones/13)

**Closed at 2021-06-10.**


### Issues
  * [[enhancement 21]](https://gitlab.com/ultreiaio/java-util/-/issues/21) **Migrate to java 11** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 22]](https://gitlab.com/ultreiaio/java-util/-/issues/22) **Introduce TimeLog (inspired by nuiton-util one, but using log4j logger instead of commons-logging)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.11](https://gitlab.com/ultreiaio/java-util/-/milestones/12)

**Closed at 2021-01-30.**


### Issues
  * [[bug 20]](https://gitlab.com/ultreiaio/java-util/-/issues/20) **Fix SortedProperties, store method uses now entrySet method** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.10](https://gitlab.com/ultreiaio/java-util/-/milestones/11)

**Closed at 2019-12-11.**


### Issues
  * [[bug 18]](https://gitlab.com/ultreiaio/java-util/-/issues/18) **Fix import manager when using inner classes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 19]](https://gitlab.com/ultreiaio/java-util/-/issues/19) **Add new method ServiceLoaders.reload** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.9](https://gitlab.com/ultreiaio/java-util/-/milestones/10)

**Closed at 2019-10-14.**


### Issues
  * [[enhancement 16]](https://gitlab.com/ultreiaio/java-util/-/issues/16) **Open SingletonSupplier constructor visibilty** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 17]](https://gitlab.com/ultreiaio/java-util/-/issues/17) **Add a method to change the supplier in SingletonSupplier** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.8](https://gitlab.com/ultreiaio/java-util/-/milestones/9)

**Closed at 2019-07-26.**


### Issues
  * [[enhancement 15]](https://gitlab.com/ultreiaio/java-util/-/issues/15) **Add io.ultreia.java4all.util.ServiceLoaders#loadUniqueService method** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.7](https://gitlab.com/ultreiaio/java-util/-/milestones/8)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.6](https://gitlab.com/ultreiaio/java-util/-/milestones/7)

**Closed at 2018-07-30.**


### Issues
  * [[enhancement 14]](https://gitlab.com/ultreiaio/java-util/-/issues/14) **Migrates to Log4J2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.5](https://gitlab.com/ultreiaio/java-util/-/milestones/6)

**Closed at *In progress*.**


### Issues
  * [[enhancement 13]](https://gitlab.com/ultreiaio/java-util/-/issues/13) **Introduce ServiceLoaders class** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.4](https://gitlab.com/ultreiaio/java-util/-/milestones/5)

**Closed at *In progress*.**


### Issues
  * [[enhancement 12]](https://gitlab.com/ultreiaio/java-util/-/issues/12) **Improve SingletonSupplier API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.3](https://gitlab.com/ultreiaio/java-util/-/milestones/4)

**Closed at *In progress*.**


### Issues
  * [[enhancement 9]](https://gitlab.com/ultreiaio/java-util/-/issues/9) **Introduce Predicates** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 10]](https://gitlab.com/ultreiaio/java-util/-/issues/10) **Introduce Comparators** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 11]](https://gitlab.com/ultreiaio/java-util/-/issues/11) **Introduce ImportManager** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.2](https://gitlab.com/ultreiaio/java-util/-/milestones/3)

**Closed at *In progress*.**


### Issues
  * [[enhancement 7]](https://gitlab.com/ultreiaio/java-util/-/issues/7) **Introduces SingletonSupplier (was previously in java-lang project)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 8]](https://gitlab.com/ultreiaio/java-util/-/issues/8) **Improve SortedProperties** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.1](https://gitlab.com/ultreiaio/java-util/-/milestones/2)

**Closed at *In progress*.**


### Issues
  * [[enhancement 6]](https://gitlab.com/ultreiaio/java-util/-/issues/6) **Introduce Enumerations** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.0](https://gitlab.com/ultreiaio/java-util/-/milestones/1)

**Closed at *In progress*.**


### Issues
  * [[enhancement 1]](https://gitlab.com/ultreiaio/java-util/-/issues/1) **Introduce SortedProperties** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 2]](https://gitlab.com/ultreiaio/java-util/-/issues/2) **Introduce RecursiveProperties** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 3]](https://gitlab.com/ultreiaio/java-util/-/issues/3) **Introduce Dates** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 4]](https://gitlab.com/ultreiaio/java-util/-/issues/4) **Introduce Zips** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 5]](https://gitlab.com/ultreiaio/java-util/-/issues/5) **Introduces GZips** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

