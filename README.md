# Java Util extends by Ultreia.io

[![Maven Central status](https://img.shields.io/maven-central/v/io.ultreia.java4all/java-util.svg)](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22io.ultreia.java4all%22%20AND%20a%3A%22java-util%22)
[![Build Status](https://gitlab.com/ultreiaio/java-util/badges/develop/pipeline.svg)](https://gitlab.com/ultreiaio/java-util/pipelines)
[![The GNU Lesser General Public License, Version 3.0](https://img.shields.io/badge/license-LGPL3-grren.svg)](http://www.gnu.org/licenses/lgpl-3.0.txt)

This project offers some classes to extends java util (not guava, nor commons).

Some of those classes are coming from nuiton-utils project.

# Resources

* [Changelog and downloads](https://gitlab.com/ultreiaio/java-util/blob/develop/CHANGELOG.md)
* [Documentation](http://ultreiaio.gitlab.io/java-util)

# Community

* [Contact](mailto:dev@tchemit.fr)
