package io.ultreia.java4all.util.json.adapters;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import io.ultreia.java4all.util.LeftOrRightContext;
import io.ultreia.java4all.util.json.JsonAdapterProvider;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created at 21/05/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.2.1
 */
public class LeftOrRightContextAdapterTest {

    public static final String JSON_CONTENT = "{\"left\":\"A\",\"right\":\"B\"}";
    public static final LeftOrRightContext<String> DATA = LeftOrRightContext.of("A", "B");
    private Gson gson;

    @Before
    public void setUp() throws Exception {
        GsonBuilder gsonBuilder = new GsonBuilder().disableHtmlEscaping();
        JsonAdapterProvider.offers(gsonBuilder::registerTypeAdapter, gsonBuilder::registerTypeHierarchyAdapter);
        gson = gsonBuilder.create();
    }

    @Test
    public void serialize() {
        String json = gson.toJson(DATA);
        assertEquals(JSON_CONTENT, json);
    }

    @Test
    public void deserialize() {
        LeftOrRightContext<String> actual = gson.fromJson(JSON_CONTENT, TypeToken.getParameterized(LeftOrRightContext.class, String.class).getType());
        assertEquals(DATA, actual);
    }
}
