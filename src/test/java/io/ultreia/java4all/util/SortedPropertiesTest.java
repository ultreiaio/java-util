package io.ultreia.java4all.util;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

/**
 * Created by tchemit on 09/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SortedPropertiesTest {

    @Test
    public void keys() {
        SortedProperties properties = new SortedProperties();
        properties.setProperty("b", "b");
        properties.setProperty("c", "c");
        properties.setProperty("d", "d");
        Assert.assertEquals(Arrays.asList("b", "c", "d"), Collections.list(properties.keys()));
        properties.setProperty("a", "a");
        Assert.assertEquals(Arrays.asList("a", "b", "c", "d"), Collections.list(properties.keys()));
    }

    @Test
    public void stringPropertyNames() {
        SortedProperties properties = new SortedProperties();
        properties.setProperty("b", "b");
        properties.setProperty("c", "c");
        properties.setProperty("d", "d");
        Assert.assertEquals(Arrays.asList("b", "c", "d"), new LinkedList<>(properties.stringPropertyNames()));
        properties.setProperty("a", "a");
        Assert.assertEquals(Arrays.asList("a", "b", "c", "d"), new LinkedList<>(properties.stringPropertyNames()));
    }

    @Test
    public void store() throws IOException {
        SortedProperties properties = new SortedProperties();
        properties.setProperty("b", "b");
        properties.setProperty("c", "c");
        properties.setProperty("d", "d");

        try (StringWriter writer = new StringWriter()) {
            properties.store(writer, "");
            String content = writer.toString();
            String[] split = content.split("\n");
            int i = 2;
            Assert.assertEquals("b=b", split[i++]);
            Assert.assertEquals("c=c", split[i++]);
            Assert.assertEquals("d=d", split[i]);
        }

        properties.setProperty("a", "a");

        try (StringWriter writer = new StringWriter()) {
            properties.store(writer, "");
            String content = writer.toString();
            String[] split = content.split("\n");
            int i = 2;
            Assert.assertEquals("a=a", split[i++]);
            Assert.assertEquals("b=b", split[i++]);
            Assert.assertEquals("c=c", split[i++]);
            Assert.assertEquals("d=d", split[i]);
        }
    }

    @Test
    public void load() throws IOException {

        SortedProperties properties = new SortedProperties();

        properties.load(new StringReader("b=b\nd=d\nc=c"));

        Assert.assertEquals(Arrays.asList("b", "c", "d"), Collections.list(properties.keys()));
    }

}
