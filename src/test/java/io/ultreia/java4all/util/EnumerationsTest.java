package io.ultreia.java4all.util;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.util.Enumeration;
import java.util.NoSuchElementException;

/**
 * Created by tchemit on 04/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class EnumerationsTest {

    @Test
    public void singleton() {
        String element = "element";
        Enumeration<String> enumeration = Enumerations.singleton(element);
        Assert.assertTrue(enumeration.hasMoreElements());
        Assert.assertEquals(element, enumeration.nextElement());
        Assert.assertFalse(enumeration.hasMoreElements());
        try {
            enumeration.nextElement();
            Assert.fail("Should not have a next element");
        } catch (NoSuchElementException e) {
            Assert.assertTrue(true);
        }
    }

}
