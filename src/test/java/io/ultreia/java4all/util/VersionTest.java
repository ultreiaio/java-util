package io.ultreia.java4all.util;

/*
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created on 7/10/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.14
 */
@SuppressWarnings("SpellCheckingInspection")
public class VersionTest {

    @Test
    public void testEquals() {

        // Ignore case on string components
        assertEquals(Version.valueOf("1.1-rc"), Version.valueOf("1.1-Rc"));
        assertEquals(Version.valueOf("1.1-m"), Version.valueOf("1.1-M"));

        // Ignore components join separator
        assertEquals(Version.valueOf("1.1"), Version.create(List.of(1, 1)).setJoinSeparator(' ').build());

    }

    /**
     * Test to create a new version by extracting a single component.
     */
    @Test
    public void testExtractSingleComponent() {
        Version initVersion = Version.valueOf("10.20.30.40");
        Version subVersion = Version.extractVersion(initVersion, 2);
        Assert.assertEquals(Version.valueOf("30"), subVersion);
    }

    /**
     * Test to create a new version by extracting a components sub set.
     */
    @Test
    public void testExtractTwoComponents() {
        Version initVersion = Version.valueOf("10.20.30.40");
        Version subVersion = Version.extractVersion(initVersion, 2, 3);
        Assert.assertEquals(Version.valueOf("30.40"), subVersion);
    }

    /**
     * Test to create a new version by extracting with illegal parameters.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testExtractIllegalParams() {
        Version initVersion = Version.valueOf("10.20.30.40");
        Version.extractVersion(initVersion, 3, 2);
    }

    @Test
    public void testIncrements() {

        Version[] versions = new Version[]{
                Version.create().setSnapshot(true).build(),
                Version.create().build(),
                Version.valueOf("1"),
                Version.valueOf("1.1-alpha-1"),
                Version.valueOf("1.1-alpha-2"),
                Version.valueOf("1.1-beta-1"),
                Version.valueOf("1.1-rc-1-SNAPSHOT"),
                Version.valueOf("1.1-rc-1"),
                Version.valueOf("1.1-rc2"),
                Version.valueOf("1.1-m1"),
                Version.valueOf("1.1-M1"),
                Version.valueOf("1.1-M-1"),
                Version.valueOf("1.1"),
                Version.valueOf("1.1.1"),
                Version.valueOf("1.1.1-blablah"),
                Version.valueOf("1.1.1-blablah1-SNAPSHOT"),
                Version.valueOf("1.1.1-blablah1"),
                Version.valueOf("1.1.1-blablah1a"),
                Version.valueOf("1.1.1-blablah1b")
        };

        Version[] versionsIncrements = new Version[]{
                Version.valueOf("1-SNAPSHOT"),
                Version.valueOf("1"),
                Version.valueOf("2"),
                Version.valueOf("1.1-alpha-2"),
                Version.valueOf("1.1-alpha-3"),
                Version.valueOf("1.1-beta-2"),
                Version.valueOf("1.1-rc-2-SNAPSHOT"),
                Version.valueOf("1.1-rc-2"),
                Version.valueOf("1.1-rc3"),
                Version.valueOf("1.1-m2"),
                Version.valueOf("1.1-M2"),
                Version.valueOf("1.1-M-2"),
                Version.valueOf("1.2"),
                Version.valueOf("1.1.2"),
                Version.valueOf("1.1.1-blablah.1"),
                Version.valueOf("1.1.1-blablah2-SNAPSHOT"),
                Version.valueOf("1.1.1-blablah2"),
                Version.valueOf("1.1.1-blablah1a.1"),
                Version.valueOf("1.1.1-blablah1b.1")
        };

        for (int i = 0, l = versions.length; i < l; i++) {
            Version v = versions[i];
            Version vIncrements = versionsIncrements[i];
            Version increments = Version.increments(v);
            Assert.assertEquals(v + " + 1 = " + increments + " should be " + vIncrements, increments, vIncrements);
            Assert.assertEquals(v + " + 1 = " + increments + " should be " + vIncrements, increments.getVersion(), vIncrements.getVersion());
        }
    }

    @Test
    public void testBuildVersionFromString() {

        buildVersion(Version.create().build(), "0", 1, false, 0);
        buildVersion(Version.create().setSnapshot(true).build(), "0-SNAPSHOT", 1, true, 0);
        buildVersion(Version.create("0").build(), "0", 1, false, 0);
        buildVersion(Version.create("1").build(), "1", 1, false, 1);
        buildVersion(Version.create("12.3").build(), "12.3", 2, false, 12, 3);
        buildVersion(Version.create("12.34-alpha-56").build(), "12.34-alpha-56", 4, false, 12, 34, "alpha", 56);
        buildVersion(Version.create("1.1-beta-1").build(), "1.1-beta-1", 4, false, 1, 1, "beta", 1);
        buildVersion(Version.create("1.1-rc-1").build(), "1.1-rc-1", 4, false, 1, 1, "rc", 1);
        buildVersion(Version.create("1.1-rc-1-SNAPSHOT").build(), "1.1-rc-1-SNAPSHOT", 4, true, 1, 1, "rc", 1);
        buildVersion(Version.create("1.1-rc2").build(), "1.1-rc2", 4, false, 1, 1, "rc", 2);
        buildVersion(Version.create("1.1-rc2a").build(), "1.1-rc2a", 5, false, 1, 1, "rc", 2, "a");
        buildVersion(Version.create("1.1-m1").build(), "1.1-m1", 4, false, 1, 1, "m", 1);

    }

    @Test
    public void testBuildVersionFromComponents() {

        buildVersion(Version.create(List.of(0)).build(), "0", 1, false, 0);
        buildVersion(Version.create(List.of(1)).build(), "1", 1, false, 1);
        buildVersion(Version.create(List.of(12, 3)).build(), "12.3", 2, false, 12, 3);
        buildVersion(Version.create(List.of(12, 34, "alpha", 56)).build(), "12.34.alpha.56", 4, false, 12, 34, "alpha", 56);
        buildVersion(Version.create(List.of(1, 1, "beta", 1)).build(), "1.1.beta.1", 4, false, 1, 1, "beta", 1);
        buildVersion(Version.create(List.of(1, 1, "rc", 1)).build(), "1.1.rc.1", 4, false, 1, 1, "rc", 1);
        buildVersion(Version.create(List.of(1, 1, "rc", 1)).setSnapshot(true).build(), "1.1.rc.1-SNAPSHOT", 4, true, 1, 1, "rc", 1);
        buildVersion(Version.create(List.of(1, 1, "rc", 2)).build(), "1.1.rc.2", 4, false, 1, 1, "rc", 2);
        buildVersion(Version.create(List.of(1, 1, "rc", 2, "a")).build(), "1.1.rc.2.a", 5, false, 1, 1, "rc", 2, "a");
        buildVersion(Version.create(List.of(1, 1, "m", 1)).build(), "1.1.m.1", 4, false, 1, 1, "m", 1);

    }


    protected void buildVersion(Version version,
                                String stringRepresentation,
                                int nbComponents,
                                boolean snapshot,
                                Comparable<?>... components) {

        Assert.assertEquals("Bad string representation", stringRepresentation, version.getVersion());
        Assert.assertEquals("Bad number of components", nbComponents, version.getComponentCount());
        Assert.assertEquals("Bad snapshot value", snapshot, version.isSnapshot());
        for (int i = 0; i < components.length; i++) {
            Comparable<?> component = components[i];
            Assert.assertEquals("Bad component value at index " + i, component, version.getComponent(i).getValue());
        }

    }

    @Test
    public void testCompareVersions() {

        Version.VersionComparator comparator = new Version.VersionComparator();

        Version[] versions = new Version[]{
                Version.create().setSnapshot(true).build(),
                Version.create().build(),
                Version.create("1").build(),
                Version.create("1.1-alpha-1").build(),
                Version.create("1.1-alpha-2").build(),
                Version.create("1.1-beta-1").build(),
                Version.create("1.1-rc-1-SNAPSHOT").build(),
                Version.create("1.1-rc-1").build(),
                Version.create("1.1-rc2").build(),
                Version.create("1.1-m1").build(),
                Version.create("1.1-M2").build(),
                Version.create("1.1-m-3").build(),
                Version.create("1.1-M-4").build(),
                Version.create("1.1").build(),
                Version.create("1.1.1").build(),
                Version.create("1.1.1-blablah").build(),
                Version.create("1.1.1-blablah1-SNAPSHOT").build(),
                Version.create("1.1.1-blablah1").build(),
                Version.create("1.1.1-blablah1a").build(),
                Version.create("1.1.1-blablah1b").build(),
                Version.create("2-rc-1").build(),
                Version.create("2").build(),
                Version.create("2-aa-1").build(),
        };

        for (int i = 0, l = versions.length - 1; i < l; i++) {

            Version v0 = versions[i];
            Version v1 = versions[i + 1];
            Assert.assertTrue(v0 + " < " + v1, comparator.compare(v0, v1) < 0);

        }

    }

    protected void assertEquals(Version v1, Version v2) {
        Assert.assertEquals(v1 + " equals to " + v2, v1, v2);
    }

}
