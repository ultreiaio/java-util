package io.ultreia.java4all.util.sql;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Test;

import java.util.Arrays;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Test the SqlScriptReader.
 *
 * @author jruchaud
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.0
 */
public class SqlScriptReaderTest {

    @Test
    public void testBasic() {
        String[] sql = {"INSERT INTO client (surname, age) VALUES ('John', 11)"};

        assertIterator(sql, sql);
    }

    @Test
    public void testBasicWithSemiColon() {
        String[] sql = {"INSERT INTO client (surname, age) VALUES ('John', 11);", ""};

        assertIterator(sql, new String[]{sql[0]});
    }

    @Test
    public void testBasicMultiInserts() {
        String[] sql = {
                "INSERT INTO client (surname, age) VALUES ('John', 11);\n\n\n\n\n",
                "INSERT INTO client (surname, age) VALUES ('Jack', 12);",
                "INSERT INTO client (surname, age) VALUES ('Bobby', 13);",
                ""};

        assertIterator(sql, new String[]{sql[0], sql[1], sql[2]});
    }

    @Test
    public void testStringStreamApi() {
        String[] sql = {
                "INSERT INTO client (surname, age) VALUES ('John', 11);\n\n\n\n\n",
                "INSERT INTO client (surname, age) VALUES ('Jack', 12);",
                "INSERT INTO client (surname, age) VALUES ('Bobby', 13);",
                "  "};

        assertIterator(SqlScriptReader.of(sql), sql[0], sql[1], sql[2]);
        assertIterator(SqlScriptReader.of(Arrays.asList(sql)), sql[0], sql[1], sql[2]);
        assertIterator(SqlScriptReader.of(Arrays.stream(sql)), sql[0], sql[1], sql[2]);
    }

    @Test
    public void testVariables() {
        String sql = "INSERT INTO client (surname, age) VALUES ('${name} and ${name} again (${surname}!)', 11);";

        // No variable
        assertIterator(SqlScriptReader.builder(sql).setVariables(Map.of()).build(), sql);
        // Variables are case sensitive
        assertIterator(SqlScriptReader.builder(sql).setVariables(Map.of("Name", "John")).build(), sql);
        // Variable found (twice) and no conflict on variable names
        assertIterator(SqlScriptReader.builder(sql).setVariables(Map.of("name", "John", "surname", "Bill")).build(), "INSERT INTO client (surname, age) VALUES ('John and John again (Bill!)', 11);");
    }

    @Test
    public void testQuoteWithSemiColon() {
        String[] sql = {"INSERT INTO client (surname, age) VALUES ('Jo;hn', 11)"};

        assertIterator(sql, sql);
    }

    @Test
    public void testQuoteWithQuote() {
        String[] sql = {"INSERT INTO client (surname, age) VALUES ('Jo''hn', 11)"};

        assertIterator(sql, sql);
    }

    @Test
    public void testQuoteMultiInserts() {
        String[] sql = {
                "INSERT INTO client (surname, age) VALUES ('John\n', 11);",
                "INSERT INTO client (surname, age) VALUES ('Jack;\n', 12);",
                "INSERT INTO client (surname, age) VALUES ('Bobby', 13);",
                ""};

        assertIterator(sql, new String[]{sql[0], sql[1], sql[2]});
    }

    @Test
    public void testComment() {
        String[] sql = {"-- Comment"};

        assertIterator(sql, new String[]{});
    }

    @Test
    public void testCommentWithSemiColon() {
        String[] sql = {"-- Comm;ent"};

        assertIterator(sql, new String[]{});
    }

    @Test
    public void testCommentMultiInserts() {
        String[] sql = {
                "INSERT INTO client (surname, age) VALUES ('John', 11);\n",
                "-- Comment\n",
                "-- Comment\n",
                "INSERT INTO client (surname, age) VALUES \n('Jack', 12);",
                "INSERT INTO client (surname, age) VALUES ('Bobby', 13);",
                ""};

        assertIterator(sql, new String[]{sql[0], sql[3], sql[4]});
    }

    private void assertIterator(String[] expectedValues, String[] expectedValuesWithoutComment) {
        assertIterator(getSqlFileReaderWithComment(expectedValues), expectedValues);
        assertIterator(getSqlFileReaderWithoutComment(expectedValues), expectedValuesWithoutComment);
    }

    private void assertIterator(SqlScriptReader reader, String... expectedValues) {
        int index = 0;
        for (String value : reader) {
            String expectedValue = expectedValues[index].replaceAll("\n*", "");
            assertEquals(expectedValue, value.replaceAll("\n*", ""));
            index++;
        }

        assertEquals(expectedValues.length, index);
        assertEquals(expectedValues.length, reader.getStatementCount());
    }

    private SqlScriptReader getSqlFileReaderWithComment(String[] sql) {
        StringBuilder content = new StringBuilder();
        for (String value : sql) {
            content.append(value);
        }
        return SqlScriptReader.builder(content.toString()).keepCommentLine().keepEmptyLine().build();
    }

    private SqlScriptReader getSqlFileReaderWithoutComment(String[] sql) {
        StringBuilder content = new StringBuilder();
        for (String value : sql) {
            content.append(value);
        }
        return SqlScriptReader.of(content.toString());
    }

}
