package io.ultreia.java4all.util.sql;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.ultreia.java4all.util.json.JsonAdapterProvider;
import io.ultreia.java4all.util.json.adapters.SqlScriptAdapter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

/**
 * Created on 13/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.0
 */
public class SqlScriptTest {

    public static final String CONTENT = "" +
            "INSERT INTO schema.table(a,b,c) VALUES('a={]r j k d  e j z l k j l  k l  é','b','c');\n" +
            "\n" +
            "\n-- My comment\n" +
            "INSERT INTO schema.table(a,b,c) VALUES('a={]r j k d  e j z l k j l  k l  é','b','c');\n" +
            "INSERT INTO schema.table(a,b,c) VALUES('a={]r j k d  e j z l k j l  k l  é','b','c');\n" +
            "INSERT INTO schema.table(a,b,c) VALUES('a={]r j k d  e j z l k j l  k l  é','b','c');\n" +
            "delete from schema.table";

    private static final Logger log = LogManager.getLogger(SqlScriptTest.class);

    @Test
    public void gson() {

        SqlScript script = SqlScript.of(CONTENT);

        String originalContent = script.content();
        log.info(String.format("Original content:(%d)\n%s", originalContent.length(), originalContent));
        GsonBuilder gsonBuilder = new GsonBuilder().disableHtmlEscaping();
        JsonAdapterProvider.offers(gsonBuilder::registerTypeAdapter, gsonBuilder::registerTypeHierarchyAdapter);
        Gson gson = gsonBuilder.create();

        String gsonContent = gson.toJson(script);

        Assert.assertNotNull(gsonContent);

        Map<?, ?> mapFromGson = gson.fromJson(gsonContent, Map.class);
        Assert.assertNotNull(mapFromGson);
        String contentFromMap = (String) mapFromGson.get(SqlScriptAdapter.LOCATION);

        log.info(String.format("Gson content:(%d)\n%s", contentFromMap.length(), contentFromMap));

        SqlScript scriptFromGson = gson.fromJson(gsonContent, SqlScript.class);
        Assert.assertNotNull(scriptFromGson);
        String contentFromGson = scriptFromGson.content();
        log.info(String.format("Content from gson:(%d)\n%s", contentFromGson.length(), contentFromGson));
        Assert.assertEquals(originalContent, contentFromGson);

    }
}
