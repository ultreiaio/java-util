package io.ultreia.java4all.util;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class ZipsTest {

    private static final Logger log = LogManager.getLogger(ZipsTest.class);
    private static File testWorkDir;
    private static File testResourcesDir;

    @BeforeClass
    public static void initTest() throws IOException {
        // get maven env basedir
        String basedir = System.getenv("basedir");
        if (basedir == null) {

            // says basedir is where we start tests.
            basedir = new File("").getAbsolutePath();
        }
        File basedirFile = new File(basedir);
        testWorkDir = new File(basedirFile, "target" + File.separator + "surefire-workdir");

        boolean b = testWorkDir.exists() || testWorkDir.mkdirs();
        if (!b) {
            throw new IOException("Could not create workdir directory " + testWorkDir);
        }
        testResourcesDir = new File(basedirFile, "src" + File.separator + "test" + File.separator + "resources" + File.separator + "zip");
    }

    /**
     * Test la fonction de decompression sans filtres.
     *
     * @throws IOException ?
     */
    @Test
    public void testUncompressFiltredWithoutFilter() throws IOException {
        File archive = new File(testResourcesDir, "test-uncompress.zip");
        File dest = new File(testWorkDir, "testunzip");

        if (log.isInfoEnabled()) {
            log.info("Extracting " + archive + " to " + dest);
        }

        Zips.uncompressFiltred(archive, dest);
        File ruleFile = new File(dest, "test-nonregression-20090203" + File.separator + "scripts" + File.separator + "RuleUtil.java");
        Assert.assertTrue(ruleFile.exists());
    }

    /**
     * Test la fonction de decompression avec filtres.
     *
     * @throws IOException ?
     */
    @Test
    public void testUncompressFiltredWithFilter() throws IOException {
        File archive = new File(testResourcesDir, "test-uncompress.zip");
        File dest = new File(testWorkDir, "testunzip2");
        String pattern = ".*/scripts/.*";

        log.info("Extracting " + archive + " to " + dest + " without " + pattern);

        Zips.uncompressFiltred(archive, dest, pattern);
        File ruleFile = new File(dest, "test-nonregression-20090203" + File.separator + "scripts" + File.separator + "RuleUtil.java");
        Assert.assertFalse(ruleFile.exists());
    }

    @Test
    public void testIsZipFile() throws Exception {

        URL resource;
        File file;
        boolean actual;

        // a zip
        resource = getClass().getResource("/zip/this-is-a-zip.zap");
        Assert.assertNotNull(resource);
        file = new File(resource.toURI());
        Assert.assertNotNull(file);
        actual = Zips.isZipFile(file);
        Assert.assertTrue(actual);

        // a zip
        resource = getClass().getResource("/zip/test-uncompress.zip");
        Assert.assertNotNull(resource);
        file = new File(resource.toURI());
        Assert.assertNotNull(file);
        actual = Zips.isZipFile(file);
        Assert.assertTrue(actual);

        // not a zip
        resource = getClass().getResource("/zip/not-a-zip.zip");
        Assert.assertNotNull(resource);
        file = new File(resource.toURI());
        Assert.assertNotNull(file);
        actual = Zips.isZipFile(file);
        Assert.assertFalse(actual);


    }
}
