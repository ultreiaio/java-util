package io.ultreia.java4all.util;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by tchemit on 30/12/2017.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DatesTest {

    private static final Logger log = LogManager.getLogger(Dates.class);

    /**
     * Test of createDate method, of class Dates.
     */
    @Test
    public void testCreateDate() {
        log.info("createDate");

        Date newDate = Dates.createDate(3, 3, 2009);
        Calendar calendar = Dates.getDefaultCalendar(newDate);

        Assert.assertEquals(3, calendar.get(Calendar.DAY_OF_MONTH));
        Assert.assertEquals(2, calendar.get(Calendar.MONTH));
        Assert.assertEquals(2009, calendar.get(Calendar.YEAR));

        Assert.assertEquals(0, calendar.get(Calendar.HOUR));
        Assert.assertEquals(0, calendar.get(Calendar.MINUTE));
        Assert.assertEquals(0, calendar.get(Calendar.SECOND));
        Assert.assertEquals(0, calendar.get(Calendar.MILLISECOND));
    }

    /**
     * Non regression test for bug #1157
     *
     * @throws ParseException ?
     * @since 1.5.2
     */
    @Test
    public void testCreateDateIsDeterministic() throws ParseException {
        Date createdDate = Dates.createDate(0, 33, 18, 22, 12, 2010);
        Date parsedDate = Dates.parseDate("22/12/2010 18:33", "dd/MM/yyyy HH:mm");
        log.info(createdDate.getTime() + " ?= " + parsedDate.getTime());
        Assert.assertEquals(createdDate, parsedDate);
    }

    /**
     * Test of setLastDayOfMonth method, of class Dates.
     */
    @Test
    public void testSetFistLastDayOfMonth() {
        Date february = Dates.createDate(22, 2, 2011);
        Date firstDayOfFebruary = Dates.setFirstDayOfMonth(february);
        Date lastDayOfFebruary = Dates.setLastDayOfMonth(february);

        Date day1 = Dates.createDate(0, 0, 2, 1, 2, 2011);
        Date day2 = Dates.createDate(67, 45, 23, 28, 2, 2011);

        if (log.isDebugEnabled()) {
            log.debug("first day of february = " + firstDayOfFebruary + ", last day of february = "
                    + lastDayOfFebruary + ", day1 = " + day1 + ", day 2 = " + day2);
        }

        Assert.assertTrue(Dates.between(day1, firstDayOfFebruary, lastDayOfFebruary));

        // In this case, day2'hour is after lastDayOfMonth
        Assert.assertFalse(Dates.between(day2, firstDayOfFebruary, lastDayOfFebruary));
    }

    /**
     * Test of setLastDayOfMonth method, of class Dates.
     */
    @Test
    public void testSetFistLastDayOfYear() {
        Date february = Dates.createDate(22, 2, 2011);
        Date firstDayOf2011 = Dates.setFirstDayOfYear(february);
        Date lastDayOf2011 = Dates.setLastDayOfYear(february);

        Date day1 = Dates.createDate(0, 0, 2, 1, 2, 2011);
        Date day2 = Dates.createDate(67, 45, 23, 28, 2, 2012);

        if (log.isDebugEnabled()) {
            log.debug("first day of february = " + firstDayOf2011 + ", last day of february = "
                    + lastDayOf2011 + ", day1 = " + day1 + ", day 2 = " + day2);
        }

        Assert.assertTrue(Dates.between(day1, firstDayOf2011, lastDayOf2011));
        Assert.assertFalse(Dates.between(day2, firstDayOf2011, lastDayOf2011));
    }

    @Test
    public void testGetDifferenceInSeconds() {
        log.info("getDifferenceInSecondes");

        Date beginDate = Dates.createDate(30, 10, 0, 3, 2, 2009);
        Date endDate = Dates.createDate(0, 11, 0, 3, 2, 2009);

        int result = Dates.getDifferenceInSeconds(beginDate, endDate);
        Assert.assertEquals(30, result);

        beginDate = Dates.createDate(9, 28, 0, 28, 1, 2009);
        endDate = Dates.createDate(50, 30, 0, 28, 1, 2009);

        result = Dates.getDifferenceInSeconds(beginDate, endDate);
        Assert.assertEquals(161, result);
    }

    @Test
    public void testGetDifferenceInMinutes() {
        log.info("getDifferenceInMinutes");

        Date beginDate = Dates.createDate(30, 10, 0, 3, 2, 2009);
        Date endDate = Dates.createDate(0, 12, 0, 3, 2, 2009);

        int result = Dates.getDifferenceInMinutes(beginDate, endDate);
        Assert.assertEquals(1, result);

        beginDate = Dates.createDate(9, 28, 0, 28, 1, 2009);
        endDate = Dates.createDate(50, 30, 0, 28, 1, 2009);

        result = Dates.getDifferenceInMinutes(beginDate, endDate);
        Assert.assertEquals(2, result);
    }

    @Test
    public void testGetDifferenceInHours() {
        log.info("getDifferenceInHours");

        Date beginDate = Dates.createDate(30, 10, 0, 3, 2, 2009);
        Date endDate = Dates.createDate(0, 11, 0, 4, 2, 2009);

        int result = Dates.getDifferenceInHours(beginDate, endDate);
        Assert.assertEquals(24, result);

        beginDate = Dates.createDate(9, 28, 0, 28, 1, 2009);
        endDate = Dates.createDate(50, 30, 8, 28, 1, 2009);

        result = Dates.getDifferenceInHours(beginDate, endDate);
        Assert.assertEquals(8, result);
    }

    @Test
    public void testGetDifferenceInDays() {
        log.info("getDifferenceInDays");

        Date beginDate = Dates.createDate(3, 2, 2009);
        Date endDate = Dates.createDate(8, 2, 2009);

        int result = Dates.getDifferenceInDays(beginDate, endDate);
        Assert.assertEquals(5, result);

        beginDate = Dates.createDate(28, 1, 2009);
        endDate = Dates.createDate(8, 2, 2009);

        result = Dates.getDifferenceInDays(beginDate, endDate);
        Assert.assertEquals(11, result);
    }

    @Test
    public void testGetDifferenceInMonths() {
        log.info("getDifferenceInMonths");

        Date beginDate = Dates.createDate(3, 2, 2009);
        Date endDate = Dates.createDate(8, 8, 2010);

        int result = Dates.getDifferenceInMonths(beginDate, endDate);
        log.info("result1 : " + result);
        Assert.assertEquals(19, result);

        beginDate = Dates.createDate(1, 1, 2009);
        endDate = Dates.createDate(28, 2, 2009);

        result = Dates.getDifferenceInMonths(beginDate, endDate);
        log.info("result2 : " + result);
        Assert.assertEquals(2, result);

        beginDate = Dates.createDate(31, 1, 2009);
        endDate = Dates.createDate(1, 2, 2009);

        result = Dates.getDifferenceInMonths(beginDate, endDate);
        log.info("result3 : " + result);
        Assert.assertEquals(1, result);
    }

    @Test
    public void testGetAge() {
        log.info("getAge");

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -20);
        calendar.add(Calendar.DAY_OF_YEAR, -1);

        int result = Dates.getAge(calendar.getTime());
        log.info("result1 : " + result);
        Assert.assertEquals(20, result);

        calendar.add(Calendar.DATE, 2);

        result = Dates.getAge(calendar.getTime());
        log.info("result2 : " + result);
        Assert.assertEquals(19, result);

        calendar.add(Calendar.YEAR, 25);

        result = Dates.getAge(calendar.getTime());
        log.info("result3 : " + result);
        Assert.assertEquals(0, result);
    }

    @Test
    public void testGetMonthLibelle() {
        log.info("getMonthLibelle");

        Locale.setDefault(Locale.FRENCH);
        String janvier = Dates.getMonthLibelle(1);
        Assert.assertEquals("janvier", janvier);

        String juli = Dates.getMonthLibelle(7, Locale.GERMAN);
        Assert.assertEquals("Juli", juli);
    }

    /**
     * Shows that {@link Dates#truncateToDayOfWeek(Date)} works
     * fine and that using commons library for this operation is not possible.
     */
    @Test
    public void testTruncateToDayOfWeek() {

        Date aDate = Dates.createDate(23, 45, 6, 19, 10, 2011);
        Date expectedWeek = Dates.createDate(17, 10, 2011);
        Date actualWeek = Dates.truncateToDayOfWeek(aDate);
        Assert.assertEquals(expectedWeek, actualWeek);
    }

    @Test
    public void testGetYesterday() {

        //Test normal yesterday
        Date aDate = Dates.createDate(3, 4, 5);
        Date expectedADate = Dates.createDate(2, 4, 5);

        //Test month change
        Date bDate = Dates.createDate(1, 12, 12);
        Date expectedBDate = Dates.createDate(30, 11, 12);

        //Test year change
        Date cDate = Dates.createDate(1, 1, 12);
        Date expectedCDate = Dates.createDate(31, 12, 11);

        //Test possible wrong change
        Date dDate = Dates.createDate(3, 1, 12);
        Date expectedDDate = Dates.createDate(2, 1, 12);


        Assert.assertEquals(expectedADate, Dates.getYesterday(aDate));
        Assert.assertEquals(expectedBDate, Dates.getYesterday(bDate));
        Assert.assertEquals(expectedCDate, Dates.getYesterday(cDate));
        Assert.assertEquals(expectedDDate, Dates.getYesterday(dDate));

    }
}
