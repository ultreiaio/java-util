package io.ultreia.java4all.util;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Created by tchemit on 09/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SingletonSupplierTest {

    @Test
    public void get() {
        List<String> hits = new LinkedList<>();

        SingletonSupplier<Long> singletonSupplier = SingletonSupplier.of(() -> {
            hits.add("yes");
            return System.nanoTime();
        });

        Long firstValue = singletonSupplier.get();
        Assert.assertNotNull(firstValue);
        Assert.assertEquals(1, hits.size());

        Assert.assertEquals(firstValue, singletonSupplier.get());
        Assert.assertEquals(1, hits.size());

        singletonSupplier.clear();

        Long secondValue = singletonSupplier.get();
        Assert.assertNotNull(secondValue);
        Assert.assertEquals(2, hits.size());
        Assert.assertFalse(Objects.equals(firstValue, secondValue));

    }

}
