package io.ultreia.java4all.util;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Represents a left and right object.
 * <p>
 * Created at 28/03/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see TwoSide
 * @see LeftOrRightContext
 * @since 1.1.16
 */
public enum LeftOrRight implements TwoSide {

    LEFT, RIGHT;

    @Override
    public boolean onLeft() {
        return LEFT == this;
    }

}
