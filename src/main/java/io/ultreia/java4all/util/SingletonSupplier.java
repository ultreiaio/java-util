package io.ultreia.java4all.util;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * Get a {@link Supplier} with a cached value acting as a singleton.
 * <p>
 * To remove cached value, use method {@link #clear()}.
 * <p>
 * Created by tchemit on 28/09/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SingletonSupplier<O> implements Supplier<O> {

    private Supplier<O> supplier;
    private final boolean requireNonNull;
    private O cache;

    protected SingletonSupplier(Supplier<O> supplier, boolean requireNonNull) {
        this.supplier = Objects.requireNonNull(supplier);
        this.requireNonNull = requireNonNull;
    }

    public static <O> SingletonSupplier<O> of(Supplier<O> supplier) {
        return of(supplier, true);
    }

    public static <O> SingletonSupplier<O> of(Supplier<O> supplier,boolean requireNonNull) {
        return new SingletonSupplier<>(supplier, requireNonNull);
    }

    @Override
    public O get() {
        return cache == null ? cache = load() : cache;
    }

    public void setSupplier(Supplier<O> supplier) {
        this.supplier = Objects.requireNonNull(supplier);
        clear();
    }

    public Optional<O> clear() {
        Optional<O> result = Optional.ofNullable(cache);
        cache = null;
        return result;
    }

    public boolean withValue() {
        return cache != null;
    }

    public Optional<O> toOptional() {
        return requireNonNull?Optional.of(get()) :Optional.ofNullable(get());
    }

    protected O load() {
        O cache = supplier.get();
        if (requireNonNull) {
            Objects.requireNonNull(cache, "Value can't not be null if requiredNonNull flag is set on.");
        }
        return cache;
    }
}
