package io.ultreia.java4all.util;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * To be able to program with natural API in a two side context.
 * <p>
 * Created at 19/03/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.13
 */
@FunctionalInterface
public interface TwoSide {

    /**
     * @return {@code} true if this object represents the left side, {@code false} otherwise.
     */
    boolean onLeft();

    /**
     * Choose the correct object for this side.
     *
     * @param leftCandidate  the left side candidate
     * @param rightCandidate the right side candidate
     * @param <O>            type of candidate
     * @return the candidate for this side
     */
    default <O> O get(O leftCandidate, O rightCandidate) {
        return (onLeft() ? leftCandidate : rightCandidate);
    }

    default <O> void accept(O o, Consumer<O> leftCandidate, Consumer<O> rightCandidate) {
        get(leftCandidate, rightCandidate).accept(o);
    }

    default <O, V> V apply(O o, Function<O, V> leftCandidate, Function<O, V> rightCandidate) {
        return get(leftCandidate, rightCandidate).apply(o);
    }

    default <O> boolean test(O o, Predicate<O> leftCandidate, Predicate<O> rightCandidate) {
        return get(leftCandidate, rightCandidate).test(o);
    }

    default void run(Runnable leftCandidate, Runnable rightCandidate) {
        get(leftCandidate, rightCandidate).run();
    }

    default <O> O call(Callable<O> leftCandidate, Callable<O> rightCandidate) throws Exception {
        return get(leftCandidate, rightCandidate).call();
    }
}
