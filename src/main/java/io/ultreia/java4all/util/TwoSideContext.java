package io.ultreia.java4all.util;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Created at 19/03/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.13
 */
public interface TwoSideContext<S extends TwoSide, O> {

    static <S extends TwoSide, O> TwoSideContext<S, O> of(O left, O right) {
        return new TwoSideContextImpl<>(left, right);
    }

    O left();

    O right();

    default O onSameSide(S side) {
        return side.get(left(), right());
    }

    default O onOppositeSide(S side) {
        return side.get(right(), left());
    }

    default <V> TwoSideContext<S, V> apply(Function<O, V> function) {
        return of(function.apply(left()), function.apply(right()));
    }

    default <V, X> TwoSideContext<S, V> apply(TwoSideContext<S, X> otherContext, BiFunction<X, O, V> function) {
        return of(function.apply(otherContext.left(), left()), function.apply(otherContext.right(), right()));
    }

    default TwoSideContext<S, O> flip() {
        return of(right(), left());
    }

    default void accept(Consumer<O> consumer) {
        consumer.accept(left());
        consumer.accept(right());
    }

    class TwoSideContextImpl<S extends TwoSide, O> implements TwoSideContext<S, O> {

        private final O left;
        private final O right;

        public TwoSideContextImpl(O left, O right) {
            this.left = Objects.requireNonNull(left);
            this.right = Objects.requireNonNull(right);
        }

        @Override
        public final O left() {
            return left;
        }

        @Override
        public final O right() {
            return right;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof TwoSideContext)) return false;
            TwoSideContext<?, ?> that = (TwoSideContext<?, ?>) o;
            return Objects.equals(left(), that.left()) && Objects.equals(right(), that.right());
        }

        @Override
        public final int hashCode() {
            return Objects.hash(left(), right());
        }
    }
}
