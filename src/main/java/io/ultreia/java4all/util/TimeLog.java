package io.ultreia.java4all.util;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.Strings;
import org.apache.logging.log4j.LogBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.util.StackLocatorUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Cette class permet de facilement trace le temps d'execution entre deux points
 * <p>
 * usage
 * <pre>
 * private final static final Log log = LogFactory.getLog(MyClass.class);
 * private final static TimeLog timeLog = new TimeLog(MyClass.class, 5000, 1000);
 *
 * long start = timeLog.getTime();
 * ...
 * // do some work
 * ...
 * start = timeLog.log(start, "MyMethod step 1", "do some work");
 * ...
 * // do some work
 * ...
 * timeLog.log(start, "MyMethod step 2", "do other work");
 *
 *
 * System.out.println ("time: " + timeLog.getCallCount());
 * </pre>
 * <p>
 * You can configure log level in configuration file with:
 * <pre>
 * log4j.logger.org.yo.MyClass=DEBUG
 * log4j.logger.org.yo.MyClass.TimeLog=INFO
 * </pre>
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 * @author Tony Chemit - dev@tchemit.fr
 * Created on 10/06/2021.
 * @since 1.0.12
 */
public class TimeLog {

    /**
     * Log used to log time
     */
    protected Logger timeLog;
    /**
     * time to trigger log time in info level (ns) (default: 1s)
     */
    protected long timeToLogInfo = 1000L * 1000000L;
    /**
     * time to trigger log time in warn level (ns) (default: 3s)
     */
    protected long timeToLogWarn = 3000L * 1000000L;
    /**
     * for each method of all proxies, keep number of call
     */
    protected Map<String, CallStat> callCount = new HashMap<>();

    public static class CallStat {

        protected long callNumber;

        protected long maxCallTime = 0;

        protected long minCallTime = Integer.MAX_VALUE;

        protected long callTime;

        @Override
        public String toString() {
            String callTimeString = Strings.convertTime(callTime);
            String avgTimeString = Strings.convertTime(callTime / callNumber);
            String minCallTimeString = Strings.convertTime(minCallTime);
            String maxCallTimeString = Strings.convertTime(maxCallTime);
            return String.format("%6s %10s %10s %10s %10s",
                                 callNumber,
                                 minCallTimeString,
                                 maxCallTimeString,
                                 avgTimeString,
                                 callTimeString);
        }

    }

    /**
     * return time in format acceptable for {@link #log(long, long, String, String)} method.
     *
     * @return the current time in nanoseconds
     */
    public static long getTime() {
        return System.nanoTime();
    }

    /**
     * @param logName log category used to log time message. This category must
     *                be category used to log message in class that use this TimeLog
     *                (normally class name). TimeLog is added at the end.
     */
    public TimeLog(String logName) {
        this(LogManager.getLogger(logName + ".TimeLog"));
    }

    /**
     * @param logName log category used to log time message. This category must
     *                be category used to log message in class that use this TimeLog
     *                (normally class name)
     * @since 2.1
     */
    public TimeLog(Class<?> logName) {
        this(logName.getName());
    }

    /**
     * @param logTime log used to log time message.
     */
    private TimeLog(Logger logTime) {
        timeLog = logTime;
    }

    /**
     * @param logName       log category used to log time message. This category must
     *                      be category used to log message in class that use this TimeLog
     *                      (normally class name)
     * @param timeToLogInfo time in millisecond after that we log info
     * @param timeToLogWarn time in millisecond after that we log warn
     */
    public TimeLog(String logName, long timeToLogInfo, long timeToLogWarn) {
        this(logName);
        setTimeToLogInfo(timeToLogInfo);
        setTimeToLogWarn(timeToLogWarn);
    }

    /**
     * @param logName       log category used to log time message. This category must
     *                      be category used to log message in class that use this TimeLog
     *                      (normally class name)
     * @param timeToLogInfo time in millisecond after that we log info
     * @param timeToLogWarn time in millisecond after that we log warn
     */
    public TimeLog(Class<?> logName, long timeToLogInfo, long timeToLogWarn) {
        this(logName.getName(), timeToLogInfo, timeToLogWarn);
    }

    /**
     * @param timeToLogInfoMs time in millisecond after that we log info
     */
    public void setTimeToLogInfo(long timeToLogInfoMs) {
        timeToLogInfo = timeToLogInfoMs * 1000000L; // convert ms → ns
    }

    /**
     * @param timeToLogWarnMs time in millisecond after that we log warn
     */
    public void setTimeToLogWarn(long timeToLogWarnMs) {
        timeToLogWarn = timeToLogWarnMs * 1000000L; // convert ms → ns
    }

    public Map<String, CallStat> getCallCount() {
        return callCount;
    }

    /**
     * add new trace, stop time is automatically computed.
     *
     * @param startNs    time returned by {@link #getTime()} method
     * @param methodName key name to store this time
     * @return time used as stop time, this permit to chain many add in same method to trace time.
     */
    public long log(long startNs, String methodName) {
        return log(startNs, getTime(), methodName, null);
    }

    /**
     * add new trace, stop time is automatically computed
     *
     * @param startNs    time returned by {@link #getTime()} method
     * @param methodName key name to store this time
     * @param msg        message to add to log
     * @return time used as stop time, this permit to chain many add in same method to trace time.
     */
    public long log(long startNs, String methodName, String msg) {
        return log(startNs, getTime(), methodName, msg);
    }

    /**
     * add new trace
     *
     * @param startNs    time returned by {@link #getTime()} method
     * @param stopNs     time returned by {@link #getTime()} method
     * @param methodName key name to store this time
     * @param msg        message to add to log
     * @return time used as stop time (stopNs)
     */
    public long log(long startNs, long stopNs, String methodName, String msg) {
        long time = stopNs - startNs;

        // increments call count for this methode
        // is not thread safe, but if we lose one or two call, is not important
        CallStat calls = callCount.computeIfAbsent(methodName, m -> new CallStat());

        calls.callNumber++;
        calls.callTime += time;

        // keep max time
        if (calls.maxCallTime < time) {
            calls.maxCallTime = time;
        }

        // keep min time
        if (calls.minCallTime > time) {
            calls.minCallTime = time;
        }

        LogBuilder logBuilder = null;
        if (time > timeToLogWarn && timeLog.isWarnEnabled()) {
            logBuilder = timeLog.atWarn();
        } else if (time > timeToLogInfo && timeLog.isInfoEnabled()) {
            logBuilder = timeLog.atInfo();
        } else if (timeLog.isDebugEnabled()) {
            logBuilder = timeLog.atDebug();
        }
        if (logBuilder != null) {
            // log message if necessary
            logBuilder.withLocation(StackLocatorUtil.getStackTraceElement(3));
            String message = computeLogMessage(methodName, msg, calls, time);
            logBuilder.log(message);
        }
        return stopNs;
    }

    public void reset() {
        getCallCount().clear();
    }

    protected String computeLogMessage(String methodName, String msg, CallStat calls, long time) {
        String timeString = Strings.convertTime(time);
        return String.format("[%10s] [%s] '%s'%s", timeString, calls, methodName, msg == null || msg.isEmpty() ? "" : (" " + msg));
    }

}
