package io.ultreia.java4all.util;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.function.Function;

/**
 * Created on 10/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.16
 */
public class RecursivePropertiesResolver {
    private static final String VARIABLE_START = "${";
    private static final String VARIABLE_END = "}";

    private final String variableStart;
    private final int variableStartLength;
    private final String variableEnd;
    private final int variableEndLength;

    public RecursivePropertiesResolver() {
        this(VARIABLE_START, VARIABLE_END);
    }

    public RecursivePropertiesResolver(String variableStart, String variableEnd) {
        this.variableStart = Objects.requireNonNull(variableStart);
        this.variableStartLength = variableStart.length();
        this.variableEnd = Objects.requireNonNull(variableEnd);
        this.variableEndLength = variableEnd.length();
    }

    public String getProperty(Function<String, String> properties, String key) {
        String result = properties.apply(key);
        if (result == null) {
            return null;
        }
        return resolve(properties, result);
    }

    public String resolve(Function<String, String> properties, String key) {
        if (key == null) {
            return null;
        }
        //Ex : result="My name is ${myName}."
        int pos = key.indexOf(variableStart);
        //Ex : pos=11
        while (pos != -1) {
            int posEnd = key.indexOf(variableEnd, pos + variableEndLength);
            //Ex : posEnd=19
            if (posEnd != -1) {
                String value = getProperty(properties, key.substring(pos + variableStartLength, posEnd));
                // Ex : getProperty("myName");
                if (value != null) {
                    // Ex : value="Doe"
                    key = key.substring(0, pos) + value + key.substring(posEnd + variableEndLength);
                    // Ex : result="My name is " + "Doe" + "."
                    pos = key.indexOf(variableStart, pos + value.length());
                    // Ex : pos=-1
                } else {
                    // Ex : value=null
                    pos = key.indexOf(variableStart, posEnd + variableEndLength);
                    // Ex : pos=-1
                }
                // Ex : pos=-1
            }
        }
        return key;
    }
}
