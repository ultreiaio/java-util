package io.ultreia.java4all.util;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Properties;

/**
 * Overrides {@link Properties} in order to check if the expected value
 * contains another property key like {@code ${...}}. It that case, the key
 * will be replaced by its value if possible.
 * <p>
 * Example :
 * <pre>
 * myFirstName=John
 * myName=Doe
 * org.nuiton.topia.userInfo.fullName=${fullName}
 * fullName=${myFirstName} ${myName}
 * namePhrase=My name is ${myName}.
 * instruction=Put your text like this : ${myText}
 * </pre>
 * <p>
 * Then:
 * <ul>
 * <li>{@code getProperty("org.nuiton.topia.userInfo.fullName")} → {@code "John Doe"}
 * <li>{@code getProperty("namePhrase")} → {@code "My name is Doe."}
 * <li>{@code getProperty("instruction")} → {@code "Put your text like this : ${myText}"}
 * </ul>
 * <p>
 * Created by tchemit on 29/12/2017.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @author Arnaud Thimel - thimel@codelutin.com
 */
public class RecursiveProperties extends Properties {

    private static final long serialVersionUID = 1L;
    private final RecursivePropertiesResolver resolver;

    public RecursiveProperties(Properties defaults, String variableStart, String variableEnd) {
        super(defaults);
        this.resolver = new RecursivePropertiesResolver(variableStart, variableEnd);
    }

    public RecursiveProperties(String variableStart, String variableEnd) {
        this.resolver = new RecursivePropertiesResolver(variableStart, variableEnd);
    }

    public RecursiveProperties() {
        this.resolver = new RecursivePropertiesResolver();
    }

    public RecursiveProperties(Properties defaults) {
        super(defaults);
        this.resolver = new RecursivePropertiesResolver();
    }

    @Override
    public String getProperty(String key) {
        return resolver.getProperty(super::getProperty, key);
    }

    public String resolve(String key) {
        return resolver.resolve(super::getProperty, key);
    }
}
