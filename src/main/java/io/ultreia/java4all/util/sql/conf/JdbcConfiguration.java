package io.ultreia.java4all.util.sql.conf;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.sql.Driver;

/**
 * Neutral Jdbc configuration.
 * <p>
 * Please use an instance of {@link JdbcConfigurationBuilder} to build a new JdbcTopiaConfiguration.
 *
 * @author Brendan Le Ny (Code Lutin)
 * @author Arnaud Thimel (Code Lutin)
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.3
 */
public interface JdbcConfiguration extends Serializable {

    String getJdbcConnectionUrl();

    String getJdbcConnectionUser();

    String getJdbcConnectionPassword();

    Class<? extends Driver> getJdbcDriverClass();

}
