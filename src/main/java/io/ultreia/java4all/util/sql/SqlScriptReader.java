package io.ultreia.java4all.util.sql;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.GZips;
import io.ultreia.java4all.util.SingletonSupplier;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;

/**
 * Create an iterable on a SQL text content. The content is iterated on each SQL
 * statement. For information the class handles semi-colon in quote.
 * <p>
 * File example:
 * INSERT INTO client (surname, age) VALUES ('John', 11);
 * INSERT INTO client (surname, age) VALUES ('Jack', 12);
 * <p>
 * Then:
 * SqlScriptReader source = new SqlScriptReader(stream);
 * for (String sql : source) {
 * // process sql variable
 * }
 * Created by tchemit on 10/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @author jruchaud
 * @since 1.1.0
 */
@SuppressWarnings("WeakerAccess")
public class SqlScriptReader implements Iterable<String>, Closeable {

    private final SingletonSupplier<Reader> source;
    private final boolean skipComment;
    private final boolean skipTrim;
    private final AtomicLong statementCount = new AtomicLong();
    private final boolean gzip;
    /**
     * Optional variables mapping to replace in statement
     *
     * @see SqlFileReaderIterator#returnStatement()
     */
    private final Map<String, String> variables;

    public static class Builder {

        private final boolean gzip;
        private final SingletonSupplier<Reader> sourceReader;
        private final Supplier<InputStream> source;
        private Charset encoding = StandardCharsets.UTF_8;
        private boolean skipComment = true;
        private boolean skipTrim = true;
        private Map<String, String> variables;

        public Builder(Supplier<InputStream> source) {
            this.source = Objects.requireNonNull(source);
            this.sourceReader = null;
            try {
                this.gzip = GZips.isGzipStream(source.get());
            } catch (IOException e) {
                throw new IllegalArgumentException("Can't determine if input stream is gzip", e);
            }
        }

        public Builder(SingletonSupplier<Reader> source, boolean gzip) {
            this.source = null;
            this.sourceReader = Objects.requireNonNull(source);
            this.gzip = gzip;
        }

        public Builder keepCommentLine() {
            skipComment = false;
            return this;
        }

        public Builder keepEmptyLine() {
            skipTrim = false;
            return this;
        }

        public Builder encoding(Charset encoding) {
            this.encoding = Objects.requireNonNull(encoding);
            return this;
        }

        public SqlScriptReader build() {
            if (source != null) {
                InputStream stream = source.get();
                try {
                    if (gzip) {
                        stream = new GZIPInputStream(this.source.get());
                    }
                } catch (IOException e) {
                    throw new IllegalStateException("Can't get gzip input stream", e);
                }
                InputStream finalStream = stream;
                return new SqlScriptReader(SingletonSupplier.of(() -> new BufferedReader(new InputStreamReader(finalStream, encoding))), skipComment, skipTrim, gzip, variables);
            }
            return new SqlScriptReader(sourceReader, skipComment, skipTrim, gzip, variables);
        }

        public Builder setVariables(Map<String, String> variables) {
            this.variables = variables;
            return this;
        }
    }

    /**
     * Use to create an iterator on the iterable.
     */
    public static class SqlFileReaderIterator implements Iterator<String> {

        private final SingletonSupplier<Reader> source;
        private final StringBuilder buffer;
        private final AtomicLong statementCount;
        /**
         * Optional variables mapping to replace in statement
         *
         * @see #returnStatement()
         */
        private final Map<String, String> variables;
        /**
         * The variable is used to keep if the iterator reach the end
         */
        private int scanner;

        private SqlFileReaderIterator(SingletonSupplier<Reader> source, AtomicLong statementCount, Map<String, String> variables) {
            this.source = source;
            this.statementCount = statementCount;
            this.variables = variables;
            this.buffer = new StringBuilder();
        }

        @Override
        public boolean hasNext() {
            return scanner != -1;
        }

        @Override
        public String next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            SqlFileParserState state = SqlFileParserState.NORMAL;
            buffer.setLength(0);
            try {
                while ((scanner = source.get().read()) != -1) {
                    char character = (char) scanner;
                    switch (state) {
                        case NORMAL:
                            switch (character) {
                                // Remove useless character
                                case '\n':
                                case '\r':
                                    break;
                                // Search the end of query
                                case ';':
                                    buffer.append(";");
                                    return returnStatement();
                                // Enter comment state if you have --
                                case '-':
                                    int length = buffer.length();
                                    if (length != 0 && buffer.charAt(length - 1) == '-') {
                                        state = SqlFileParserState.COMMENT;
                                    }
                                    buffer.append(character);
                                    break;
                                // Enter quote state
                                case '\'':
                                    state = SqlFileParserState.QUOTE;
                                    buffer.append(character);
                                    break;
                                // By default append character
                                default:
                                    buffer.append(character);
                                    break;
                            }
                            break;
                        case QUOTE:
                            if (character == '\'') {
                                // Remove useless character
                                // Search the end of quote
                                state = SqlFileParserState.NORMAL;
                                buffer.append(character);
                            } else {
                                // By default append character
                                buffer.append(character);
                            }
                            break;
                        case COMMENT:
                            switch (character) {
                                // Search the end of comment
                                case '\n':
                                case '\r':
                                    return returnStatement();
                                // By default append character
                                default:
                                    buffer.append(character);
                                    break;
                            }
                            break;
                    }
                }
            } catch (IOException ex) {
                throw new IllegalStateException(ex);
            }
            return returnStatement();
        }

        private String returnStatement() {
            statementCount.incrementAndGet();
            String statement = buffer.toString();
            return replaceVariablesInStatement(statement);
        }

        private String replaceVariablesInStatement(String statement) {
            if (variables != null && statement.contains("${")) {
                for (Map.Entry<String, String> entry : variables.entrySet()) {
                    String variable = entry.getKey();
                    String value = entry.getValue();
                    statement = statement.replaceAll(variable, value);
                }
            }
            return statement;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("remove");
        }
    }

    public static class SqlFileReaderIteratorWithoutComment implements Iterator<String> {

        private final SqlFileReaderIterator delegate;
        private final AtomicLong statementCount;
        private final boolean skipComment;
        private final boolean skipTrim;
        private String next;

        private SqlFileReaderIteratorWithoutComment(SingletonSupplier<Reader> source, AtomicLong statementCount, boolean skipComment, boolean skipTrim, Map<String, String> variables) {
            this.delegate = new SqlFileReaderIterator(source, new AtomicLong(0), variables);
            this.statementCount = statementCount;
            this.skipComment = skipComment;
            this.skipTrim = skipTrim;
        }

        @Override
        public boolean hasNext() {
            if (next != null) {
                return true;
            }
            boolean hasNext = delegate.hasNext();
            if (hasNext) {
                next = delegate.next();
                if (skipTrim && next.trim().isEmpty()) {
                    next = null;
                    return hasNext();
                }
                if (skipComment && next.trim().startsWith("--")) {
                    next = null;
                    return hasNext();
                }
            }
            return hasNext;
        }

        @Override
        public String next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            statementCount.incrementAndGet();
            String next = this.next;
            this.next = null;
            return next;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("remove");
        }
    }

    public static class StringStreamReader extends BufferedReader {

        public StringStreamReader(Stream<String> data) {
            super(new InputStreamReader(new ConcatenatedInputStream(data.map(s -> new ByteArrayInputStream(s.getBytes())))));
        }
    }

    public static class ConcatenatedInputStream extends InputStream {

        /**
         * EOF
         */
        public static final int EOF = -1;

        private static final InputStream AT_EOF = new InputStream() {
            public int read() {
                return EOF;
            }
        };
        private final Iterator<InputStream> iter;
        private InputStream current;

        /**
         * Create a new ConcatenatedInputStream.
         *
         * @param src InputStreams
         */
        public ConcatenatedInputStream(Stream<InputStream> src) {
            this.iter = Objects.requireNonNull(src).iterator();
            next();
        }

        @Override
        public int read() throws IOException {
            int n = current.read();
            while (n == EOF && current != AT_EOF) {
                next();
                n = current.read();
            }
            return n;
        }

        /**
         * Position to the next InputStream
         */
        private void next() {
            current = iter.hasNext() ? iter.next() : AT_EOF;
        }

    }

    public static SqlScriptReader of(String source) {
        return builder(Objects.requireNonNull(source)).build();
    }

    public static SqlScriptReader of(String[] source) {
        return builder(Objects.requireNonNull(source)).build();
    }

    public static SqlScriptReader of(Collection<String> source) {
        return builder(Objects.requireNonNull(source)).build();
    }

    public static SqlScriptReader of(Stream<String> source) {
        return builder(Objects.requireNonNull(source)).build();
    }

    public static SqlScriptReader of(byte[] source) {
        return builder(Objects.requireNonNull(source)).build();
    }

    public static SqlScriptReader of(Path source) {
        return builder(Objects.requireNonNull(source)).build();
    }

    public static SqlScriptReader of(URL source) {
        return builder(Objects.requireNonNull(source)).build();
    }

    public static SqlScriptReader of(InputStream source) {
        return builder(source).build();
    }

    public static Builder builder(String source) {
        return builder(Objects.requireNonNull(source).getBytes());
    }

    public static Builder builder(String[] source) {
        return builder(Arrays.stream(Objects.requireNonNull(source)));
    }

    public static Builder builder(Collection<String> source) {
        return builder(Objects.requireNonNull(source).stream());
    }

    public static Builder builder(Stream<String> source) {
        return new Builder(SingletonSupplier.of(() -> new StringStreamReader(Objects.requireNonNull(source))), false);
    }

    public static Builder builder(InputStream content) {
        return new Builder(() -> new BufferedInputStream(Objects.requireNonNull(content)));
    }

    public static Builder builder(byte[] source) {
        return new Builder(() -> new BufferedInputStream(new ByteArrayInputStream(Objects.requireNonNull(source))));
    }

    public static Builder builder(Path source) {
        return new Builder(() -> {
            try {
                return new BufferedInputStream(Files.newInputStream(Objects.requireNonNull(source)));
            } catch (IOException e) {
                throw new IllegalArgumentException("Can't get input stream from source: " + source, e);
            }
        });
    }

    public static Builder builder(URL source) {
        return new Builder(() -> {
            try {
                return new BufferedInputStream(Objects.requireNonNull(source).openStream());
            } catch (IOException e) {
                throw new IllegalArgumentException("Can't get input stream from source: " + source, e);
            }
        });
    }

    public static Builder builder(SqlScriptReader location) {
        return new Builder(location.getSource(), location.isGzip());
    }

    private SqlScriptReader(SingletonSupplier<Reader> source, boolean skipComment, boolean skipTrim, boolean gzip, Map<String, String> variables) {
        this.source = source;
        this.skipComment = skipComment;
        this.skipTrim = skipTrim;
        this.gzip = gzip;
        if (variables != null) {
            // we immediately compute variable pattern to search in statements
            Map<String, String> builder = new LinkedHashMap<>();
            variables.forEach((k, v) -> builder.put(String.format("\\$\\{%s}", k), v));
            variables = builder;
            if (variables.isEmpty()) {
                variables = null;
            }
        }
        this.variables = variables;
    }

    @Override
    public Iterator<String> iterator() {
        return skipComment || skipTrim ? new SqlFileReaderIteratorWithoutComment(source, statementCount, skipComment, skipTrim, variables) : new SqlFileReaderIterator(source, statementCount, variables);
    }

    public long getStatementCount() {
        return statementCount.get();
    }

    public boolean isGzip() {
        return gzip;
    }

    public SingletonSupplier<Reader> getSource() {
        return source;
    }

    @Override
    public void close() throws IOException {
        if (source.withValue()) {
            source.get().close();
        }
    }

    enum SqlFileParserState {NORMAL, QUOTE, COMMENT}

}
