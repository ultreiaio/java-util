package io.ultreia.java4all.util.sql.conf;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.sql.Driver;

/**
 * Builder to create a {@link JdbcConfiguration} instance
 */
public class JdbcConfigurationBuilder {

    @SuppressWarnings("unchecked")
    public JdbcConfiguration forDatabase(String jdbcConnectionUrl, String jdbcConnectionUser, String jdbcConnectionPassword, String jdbcDriverClassName) {
        try {
            Class<?> jdbcDriverClass = Class.forName(jdbcDriverClassName);
            if (Driver.class.isAssignableFrom(jdbcDriverClass)) {
                return forDatabase(jdbcConnectionUrl, jdbcConnectionUser, jdbcConnectionPassword, (Class<? extends Driver>) jdbcDriverClass);
            } else {
                throw new IllegalArgumentException(jdbcDriverClassName + " seems not to be a JDBC driver: it does not implement " + Driver.class.getName());
            }
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException("unable to find JDBC driver class " + jdbcDriverClassName + " in classpath", e);
        }
    }

    public JdbcConfiguration forDatabase(String jdbcConnectionUrl, String jdbcConnectionUser, String jdbcConnectionPassword, Class<? extends Driver> jdbcDriverClass) {
        BeanJdbcConfiguration beanJdbcConfiguration = new BeanJdbcConfiguration();
        beanJdbcConfiguration.setJdbcConnectionUrl(jdbcConnectionUrl);
        beanJdbcConfiguration.setJdbcConnectionUser(jdbcConnectionUser);
        beanJdbcConfiguration.setJdbcConnectionPassword(jdbcConnectionPassword);
        beanJdbcConfiguration.setJdbcDriverClass(jdbcDriverClass);
        return beanJdbcConfiguration;
    }

    public JdbcConfiguration forDatabase(String jdbcConnectionUrl, String jdbcConnectionUser, String jdbcConnectionPassword) {
        String guessedJdbcDriverClassName = guessJdbcDriverClassName(jdbcConnectionUrl);
        if (guessedJdbcDriverClassName == null) {
            throw new UnsupportedOperationException("unable to guess JDBC driver class name for URL " + jdbcConnectionUrl);
        }
        return forDatabase(jdbcConnectionUrl, jdbcConnectionUser, jdbcConnectionPassword, guessedJdbcDriverClassName);
    }

    public JdbcConfiguration forPostgresqlDatabase(String jdbcConnectionUrl, String jdbcConnectionUser, String jdbcConnectionPassword) {
        return forDatabase(jdbcConnectionUrl, jdbcConnectionUser, jdbcConnectionPassword, getPostgreSqlJdbcDriverClassName());
    }

    public JdbcConfiguration forH2Database(String jdbcConnectionUrl, String jdbcConnectionUser, String jdbcConnectionPassword) {
        return forDatabase(jdbcConnectionUrl, jdbcConnectionUser, jdbcConnectionPassword, getH2JdbcDriverClassName());
    }

    public String guessJdbcDriverClassName(String jdbcUrl) {
        String guessedJdbcDriverClassName;
        if (isDb2Url(jdbcUrl)) {
            guessedJdbcDriverClassName = getDb2JdbcDriverClassName();
        } else if (isDerbyUrl(jdbcUrl)) {
            guessedJdbcDriverClassName = getDerbyJdbcDriverClassName();
        } else if (isH2Url(jdbcUrl)) {
            guessedJdbcDriverClassName = getH2JdbcDriverClassName();
        } else if (isHsqlDbUrl(jdbcUrl)) {
            guessedJdbcDriverClassName = getHqlDbJdbcDriverClassName();
        } else if (isMysqlUrl(jdbcUrl)) {
            guessedJdbcDriverClassName = getMysqlJdbcDriverClassName();
        } else if (isMariaDbUrl(jdbcUrl)) {
            guessedJdbcDriverClassName = getMariaDbJdbcDriverClassName();
        } else if (isGoogleAppEngineUrl(jdbcUrl)) {
            guessedJdbcDriverClassName = getGooglaAppEngineJdbcDriverClassName();
        } else if (isOracleUrl(jdbcUrl)) {
            guessedJdbcDriverClassName = getOracleJdbcDriverClassName();
        } else if (isPostgreSqlUrl(jdbcUrl)) {
            guessedJdbcDriverClassName = getPostgreSqlJdbcDriverClassName();
        } else if (isJtdsUrl(jdbcUrl)) {
            guessedJdbcDriverClassName = getJdtsJdbcDriverClassName();
        } else if (isSqlServerUrl(jdbcUrl)) {
            guessedJdbcDriverClassName = getSqlServerJdbcDriverClassName();
        } else if (isSqliteUrl(jdbcUrl)) {
            guessedJdbcDriverClassName = getSqliteJdbcDriverClassName();
        } else if (isSqlDroidUrl(jdbcUrl)) {
            guessedJdbcDriverClassName = getSqlDroidJdbcDriverClassName();
        } else {
            guessedJdbcDriverClassName = null;
        }
        return guessedJdbcDriverClassName;
    }

    public String getDb2JdbcDriverClassName() {
        return "com.ibm.db2.jcc.DB2Driver";
    }

    public String getDerbyJdbcDriverClassName() {
        return "org.apache.derby.jdbc.EmbeddedDriver";
    }

    public String getH2JdbcDriverClassName() {
        return "org.h2.Driver";
    }

    public String getHqlDbJdbcDriverClassName() {
        return "org.hsqldb.jdbcDriver";
    }

    public String getMysqlJdbcDriverClassName() {
        return "com.mysql.jdbc.Driver";
    }

    public String getOracleJdbcDriverClassName() {
        return "oracle.jdbc.OracleDriver";
    }

    public String getGooglaAppEngineJdbcDriverClassName() {
        return "com.google.appengine.api.rdbms.AppEngineDriver";
    }

    public String getMariaDbJdbcDriverClassName() {
        return "org.mariadb.jdbc.Driver";
    }

    public String getPostgreSqlJdbcDriverClassName() {
        return "org.postgresql.Driver";
    }

    public String getJdtsJdbcDriverClassName() {
        return "net.sourceforge.jtds.jdbc.Driver";
    }

    public String getSqlServerJdbcDriverClassName() {
        return "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    }

    public String getSqliteJdbcDriverClassName() {
        return "org.sqlite.JDBC";
    }

    public String getSqlDroidJdbcDriverClassName() {
        return "org.sqldroid.SQLDroidDriver";
    }

    public static boolean isDb2Url(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:db2:");
    }

    public static boolean isSqlDroidUrl(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:sqldroid:");
    }

    public static boolean isSqliteUrl(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:sqlite:");
    }

    public static boolean isSqlServerUrl(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:sqlserver:");
    }

    public static boolean isJtdsUrl(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:jtds:");
    }

    public static boolean isPostgreSqlUrl(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:postgresql:");
    }

    public static boolean isOracleUrl(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:oracle:");
    }

    public static boolean isGoogleAppEngineUrl(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:google:");
    }

    public static boolean isMariaDbUrl(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:mariadb:");
    }

    public static boolean isMysqlUrl(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:mysql:");
    }

    public static boolean isHsqlDbUrl(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:hsqldb:");
    }

    public static boolean isH2Url(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:h2:");
    }

    public static boolean isDerbyUrl(String jdbcUrl) {
        return jdbcUrl.startsWith("jdbc:derby:");
    }
}
