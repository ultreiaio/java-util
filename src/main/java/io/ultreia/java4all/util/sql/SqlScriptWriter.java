package io.ultreia.java4all.util.sql;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.zip.GZIPOutputStream;

/**
 * Created by tchemit on 05/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.0
 */
@SuppressWarnings("WeakerAccess")
public class SqlScriptWriter implements Closeable {

    private final Supplier<OutputStream> target;
    private final boolean skipComment;
    private final boolean skipTrim;
    private final BufferedWriter writer;
    private long statementCount;

    public static class Builder {

        private final Supplier<OutputStream> source;
        private Charset encoding = StandardCharsets.UTF_8;
        private boolean skipComment = true;
        private boolean skipTrim = true;
        private boolean gzip;

        public Builder(Supplier<OutputStream> source) {
            this.source = Objects.requireNonNull(source);
        }

        public Builder gzip() {
            this.gzip = true;
            return this;
        }

        public Builder keepCommentLine() {
            this.skipComment = false;
            return this;
        }

        public Builder keepEmptyLine() {
            this.skipTrim = false;
            return this;
        }

        public Builder encoding(Charset encoding) {
            this.encoding = Objects.requireNonNull(encoding);
            return this;
        }

        public SqlScriptWriter build() {
            return new SqlScriptWriter(source, skipComment, skipTrim, gzip, encoding);
        }
    }

    public static SqlScriptWriter of(Path target) {
        return builder(target).build();
    }

    public static SqlScriptWriter of(OutputStream target) {
        return builder(target).build();
    }

    public static Builder builder(Path target) {
        return new Builder(() -> {
            try {
                return Files.newOutputStream(target);
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        });
    }

    public static Builder builder(OutputStream target) {
        return new Builder(() -> target);
    }

    private SqlScriptWriter(Supplier<OutputStream> target, boolean skipComment, boolean skipTrim, boolean gzip, Charset encoding) {
        this.target = target;
        this.skipComment = skipComment;
        this.skipTrim = skipTrim;
        OutputStream stream = target.get();
        try {
            if (gzip) {
                stream = new GZIPOutputStream(stream);
            }
        } catch (IOException e) {
            throw new IllegalStateException("Can't get output stream from: " + target, e);
        }
        this.writer = new BufferedWriter(new OutputStreamWriter(stream, encoding));
    }

    public void writeSql(String sql) {
        if (skipTrim && sql.trim().isEmpty()) {
            return;
        }
        if (skipComment && sql.trim().startsWith("--")) {
            return;
        }
        try {
            writer.write(sql);
            if (!sql.endsWith(";")) {
                writer.write(";");
            }
            writer.newLine();
        } catch (IOException e) {
            throw new IllegalStateException(String.format("can't write sql: [%s] to path: %s", sql, target), e);
        }
        statementCount++;
    }

    public long getStatementCount() {
        return statementCount;
    }

    @Override
    public void close() throws IOException {
        writer.close();
    }

    public void flush() throws IOException {
        writer.flush();
    }

    public void writeScript(Path sqlScriptFile) throws IOException {
        SqlScriptReader.Builder builder = SqlScriptReader.builder(sqlScriptFile);
        if (!skipComment) {
            builder.keepCommentLine();
        }
        if (!skipTrim) {
            builder.keepEmptyLine();
        }
        try (SqlScriptReader sqlScripReader = builder.build()) {
            for (String statement : sqlScripReader) {
                writeSql(statement);
            }
        }
    }

    public void writeScript(SqlScriptReader sqlScripReader) throws IOException {
        for (String statement : sqlScripReader) {
            writeSql(statement);
        }
    }
}
