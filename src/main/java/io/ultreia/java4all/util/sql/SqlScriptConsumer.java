package io.ultreia.java4all.util.sql;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.SingletonSupplier;
import io.ultreia.java4all.util.TimeLog;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.rowset.serial.SerialBlob;
import java.io.Closeable;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;

/**
 * To consume an incoming sql script into a topia persistence database.
 * <p>
 * Created by tchemit on 10/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.0
 */
public class SqlScriptConsumer implements SqlWork, Closeable, Consumer<Connection> {

    private static final Logger log = LogManager.getLogger(SqlScriptConsumer.class);
    private static final TimeLog TIME_LOG = new TimeLog(SqlScriptConsumer.class, 100, 500);
    private final SingletonSupplier<SqlScriptReader> source;
    private final Set<BlobsContainer> blobsContainers;
    private final Integer batchSize;
    private final boolean commit;
    private final List<String> statements;
    private long statementCount;

    public static class Builder {

        private final SqlScriptReader.Builder source;
        private final Set<BlobsContainer> blobsContainers;
        private Integer batchSize;

        private boolean commit;

        public Builder(SqlScriptReader.Builder source) {
            this.source = Objects.requireNonNull(source);
            this.blobsContainers = new LinkedHashSet<>();
        }

        public Builder batchSize(int batchSize) {
            this.batchSize = batchSize;
            return this;
        }

        public Builder keepCommentLine() {
            source.keepCommentLine();
            return this;
        }

        public Builder commit() {
            this.commit = true;
            return this;
        }

        public Builder keepEmptyLine() {
            source.keepEmptyLine();
            return this;
        }

        public Builder encoding(Charset encoding) {
            source.encoding(Objects.requireNonNull(encoding));
            return this;
        }

        public Builder blob(BlobsContainer blobsContainers) {
            this.blobsContainers.add(Objects.requireNonNull(blobsContainers));
            return this;
        }

        public Builder blobs(Collection<BlobsContainer> blobsContainers) {
            this.blobsContainers.addAll(Objects.requireNonNull(blobsContainers));
            return this;
        }

        public SqlScriptConsumer build() {
            return new SqlScriptConsumer(SingletonSupplier.of(source::build), batchSize, commit, Collections.unmodifiableSet(blobsContainers));
        }
    }

    public static SqlScriptConsumer of(String source) {
        return builder(source).build();
    }

    public static SqlScriptConsumer of(byte[] source) {
        return builder(source).build();
    }

    public static SqlScriptConsumer of(Path source) {
        return builder(source).build();
    }

    public static SqlScriptConsumer of(URL source) {
        return builder(source).build();
    }

    public static SqlScriptConsumer of(SqlScript source) {
        return builder(source).build();
    }

    public static Builder builder(String source) {
        return new Builder(SqlScriptReader.builder(source));
    }

    public static Builder builder(byte[] source) {
        return new Builder(SqlScriptReader.builder(source));
    }

    public static Builder builder(Path source) {
        return new Builder(SqlScriptReader.builder(source));
    }

    public static Builder builder(URL source) {
        return new Builder(SqlScriptReader.builder(source));
    }

    public static Builder builder(SqlScript source) {
        Builder builder = builder(source.getLocation());
        if (source.withBlobs()) {
            builder.blobs(source.getBlobsContainers());
        }
        return builder;
    }

    public static Builder builder(SqlScriptReader source) {
        return new Builder(SqlScriptReader.builder(source));
    }

    private SqlScriptConsumer(SingletonSupplier<SqlScriptReader> source, Integer batchSize, boolean commit, Set<BlobsContainer> blobsContainers) {
        this.source = source;
        this.batchSize = batchSize;
        this.statements = new ArrayList<>(batchSize == null ? 1 : batchSize);
        this.commit = commit;
        this.blobsContainers = blobsContainers;
    }

    @Override
    public void close() throws IOException {
        if (source.withValue()) {
            source.get().close();
        }
    }

    @Override
    public void execute(Connection connection) throws SQLException {
        boolean autoCommit = connection.getAutoCommit();
        try {
            connection.setAutoCommit(false);
            try (Statement statement = connection.createStatement()) {
                if (batchSize == null) {
                    executeOneByOne(statement);
                } else {
                    executeByBatch(statement);
                }
            }
            if (!blobsContainers.isEmpty()) {
                importBlobs(connection);
            }
            if (commit) {
                connection.commit();
            }
        } finally {
            connection.setAutoCommit(autoCommit);
        }
    }

    @Override
    public void accept(Connection connection) {
        try {
            execute(connection);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public long getStatementCount() {
        return statementCount;
    }

    private void importBlobs(Connection connection) throws SQLException {

        for (BlobsContainer blobsContainer : blobsContainers) {

            String tableName = blobsContainer.getTableName();
            String columnName = blobsContainer.getColumnName();
            int batchSize = 0;

            String sql = String.format("UPDATE %s SET %s = ? WHERE topiaId= ?", tableName, columnName);
            log.debug(sql);
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                for (Map.Entry<String, byte[]> containerEntry : blobsContainer.getBlobsById().entrySet()) {

                    String topiaId = containerEntry.getKey();
                    byte[] content = containerEntry.getValue();

                    statement.clearParameters();
                    statement.setBlob(1, new SerialBlob(content));
                    statement.setString(2, topiaId);
                    statement.addBatch();

                    batchSize++;
                    statementCount++;
                    if (this.batchSize == null || batchSize % this.batchSize == 0) {
                        flush(statement);
                    }
                }
                flush(statement);
            }
        }
    }

    private void executeByBatch(Statement statement) throws SQLException {
        for (String sqlStatement : source.get()) {
            statementCount++;
            try {
                statement.addBatch(sqlStatement);
                statements.add(sqlStatement);
            } catch (Exception e) {
                log.error(String.format("Can't add sql statement (%d) in batch: %s", statementCount, sqlStatement), e);
                throw e;
            }
            if (statementCount > 0 && statementCount % batchSize == 0) {
                flush(statement);
            }
        }
        flush(statement);
    }

    private void executeOneByOne(Statement statement) throws SQLException {

        for (String sqlStatement : source.get()) {
            long t0 = TimeLog.getTime();
            statementCount++;
            try {
                statement.execute(sqlStatement);
            } catch (Exception e) {
                log.error(String.format("Can't execute sql statement (%d): %s", statementCount, sqlStatement), e);
                throw e;
            } finally {
                TIME_LOG.log(t0, "executeOneByOne", String.format("statementCount: %d (%s)", statementCount, sqlStatement));
            }
        }
    }

    private void flush(Statement statement) throws SQLException {
        long t0 = TimeLog.getTime();
        try {
            statement.executeBatch();
        } catch (Exception e) {
            log.error(String.format("Can't execute sql statements (%d): %s", statementCount, statements), e);
            throw e;
        } finally {
            TIME_LOG.log(t0, "flush", String.format("statementCount: %d (batchSize: %d)", statementCount, batchSize));
            statements.clear();
            statement.clearBatch();
        }
    }
}
