package io.ultreia.java4all.util.sql;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Pour contenir des blobs pour une colonne de type blob sur une table donnée.
 * <p>
 * Created on 24/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.0
 */
public class BlobsContainer {

    private final String tableName;
    private final String columnName;
    private final HashMap<String, byte[]> blobsById;

    public static class Builder {

        private final String tableName;
        private final String columnName;
        private final HashMap<String, byte[]> blobsContainer = new HashMap<>();

        private Builder(String tableName, String columnName) {
            this.tableName = tableName;
            this.columnName = columnName;
        }

        public Builder addBlob(String id, byte[] content) {
            blobsContainer.put(id, content);
            return this;
        }

        public String getTableName() {
            return tableName;
        }

        public String getColumnName() {
            return columnName;
        }

        public BlobsContainer build() {
            return new BlobsContainer(tableName, columnName, blobsContainer);
        }
    }

    public static Builder builder(String tableName, String columnName) {
        return new Builder(tableName, columnName);
    }

    public BlobsContainer(String tableName, String columnName, HashMap<String, byte[]> blobsById) {
        this.tableName = Objects.requireNonNull(tableName);
        this.columnName = Objects.requireNonNull(columnName);
        this.blobsById = Objects.requireNonNull(blobsById);
    }

    public BlobsContainer add(Map<String, byte[]> blobsById) {
        HashMap<String, byte[]> newMap = new HashMap<>(this.blobsById.size() + blobsById.size());
        newMap.putAll(this.blobsById);
        newMap.putAll(blobsById);
        return new BlobsContainer(tableName, columnName, newMap);
    }

    public String getTableName() {
        return tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public HashMap<String, byte[]> getBlobsById() {
        return blobsById;
    }

    public boolean isEmpty() {
        return blobsById.isEmpty();
    }
}
