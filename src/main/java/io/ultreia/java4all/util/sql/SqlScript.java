package io.ultreia.java4all.util.sql;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.json.JsonAware;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Supplier;

/**
 * Created by tchemit on 14/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.0
 */
public class SqlScript implements JsonAware {

    protected Supplier<SqlScriptReader> location;
    protected Set<BlobsContainer> blobsContainers;

    public static SqlScript of(Path location) {
        try {
            return of(location.toUri().toURL());
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static SqlScript of(URI location) {
        try {
            return of(location.toURL());
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static SqlScript of(byte... location) {
        return new SqlScript(() -> SqlScriptReader.of(location));
    }

    public static SqlScript of(String location) {
        return new SqlScript(() -> SqlScriptReader.of(location));
    }

    public static SqlScript of(URL location) {
        return new SqlScript(() -> SqlScriptReader.of(location));
    }

    protected SqlScript(Supplier<SqlScriptReader> location) {
        this.location = location;
    }

    public SqlScript addBlobsContainer(BlobsContainer blobsContainer) {
        getBlobsContainers().add(blobsContainer);
        return this;
    }

    public SqlScript addBlobsContainers(Collection<BlobsContainer> blobsContainers) {
        getBlobsContainers().addAll(blobsContainers);
        return this;
    }

    public Set<BlobsContainer> getBlobsContainers() {
        return blobsContainers == null ? blobsContainers = new LinkedHashSet<>() : blobsContainers;
    }

    public SqlScriptReader getLocation() {
        return location.get();
    }

    public void setLocation(Supplier<SqlScriptReader> location) {
        this.location = location;
    }

    /**
     * Copy current script with no compress.
     *
     * @param target where to create script
     * @throws IOException if any io exception
     */
    public void copy(Path target) throws IOException {
        try (SqlScriptWriter scriptWriter = SqlScriptWriter.of(target)) {
            copy(scriptWriter);
        }
    }

    /**
     * Copy current script with no compress.
     *
     * @param target where to create script
     * @throws IOException if any io exception
     */
    public void copy(OutputStream target) throws IOException {
        try (SqlScriptWriter scriptWriter = SqlScriptWriter.of(target)) {
            copy(scriptWriter);
        }
    }

    /**
     * Copy current script and compress it (with gzip).
     *
     * @param target where to create script
     * @throws IOException if any io exception
     */
    public void copyAndCompress(Path target) throws IOException {
        try (SqlScriptWriter scriptWriter = SqlScriptWriter.builder(target).gzip().build()) {
            copy(scriptWriter);
        }
    }

    public void copy(SqlScriptWriter scriptWriter) throws IOException {
        try (SqlScriptReader scriptReader = location.get()) {
            for (String statement : scriptReader) {
                scriptWriter.writeSql(statement);
            }
        }
    }

    public String content() {
        try (ByteArrayOutputStream target = new ByteArrayOutputStream()) {
            try (BufferedOutputStream buffer = new BufferedOutputStream(target)) {
                copy(buffer);
            }
            return target.toString(StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new IllegalStateException("Can't copy script content", e);
        }
    }

    public byte[] toByteArray() {
        try (ByteArrayOutputStream target = new ByteArrayOutputStream()) {
            try (BufferedOutputStream buffer = new BufferedOutputStream(target)) {
                copy(buffer);
            }
            return target.toByteArray();
        } catch (IOException e) {
            throw new IllegalStateException("Can't copy script content", e);
        }
    }

    public boolean withBlobs() {
        return blobsContainers != null && blobsContainers.size() > 1;
    }
}
