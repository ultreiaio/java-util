package io.ultreia.java4all.util;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Enumeration;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Created by tchemit on 04/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class Enumerations {

    public static <T> Stream<T> stream(Enumeration<T> e) {
        return stream(e, false);
    }

    public static <T> Stream<T> stream(Enumeration<T> e, boolean parallel) {
        Objects.requireNonNull(e);
        return StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(
                        new Iterator<T>() {
                            public T next() {
                                return e.nextElement();
                            }

                            public boolean hasNext() {
                                return e.hasMoreElements();
                            }
                        },
                        Spliterator.ORDERED), parallel);
    }

    public static <O> Enumeration<O> singleton(O element) {
        return new SingletonEnumeration<>(element);
    }

    /**
     * Creates a new enumeration with a single element.
     *
     * @param <O> type of enumeration objects
     */
    private static class SingletonEnumeration<O> implements Enumeration<O> {
        private final O element;
        private boolean nextElement;

        public SingletonEnumeration(O element) {
            Objects.requireNonNull(element);
            this.element = element;
            this.nextElement = true;
        }

        @Override
        public boolean hasMoreElements() {
            return nextElement;
        }

        @Override
        public O nextElement() {
            if (!hasMoreElements()) {
                throw new NoSuchElementException();
            }
            nextElement = false;
            return element;
        }
    }

}
