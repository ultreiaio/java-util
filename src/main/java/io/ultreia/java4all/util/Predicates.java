package io.ultreia.java4all.util;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * Created by tchemit on 20/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class Predicates {

    /**
     * Compute a and predicate of given {@code predicates}.
     *
     * @param predicates predicates to concat
     * @param <O>        type of object of predicates
     * @return the concatenated predicate or {@code d -> true} predicate if collection is empty
     */
    public static <O> Predicate<O> predicate(Collection<Predicate<O>> predicates) {
        Iterator<Predicate<O>> iterator = Objects.requireNonNull(predicates).iterator();
        if (!iterator.hasNext()) {
            return d -> true;
        }
        Predicate<O> result = iterator.next();
        while (iterator.hasNext()) {
            result = result.and(iterator.next());
        }
        return result;
    }

    public static <O> Predicate<O> Equals(O value) {
        return new Predicates.EqualsPredicate<>(value);
    }

    public static <O> Predicate<O> NotEquals(O value) {
        return Equals(value).negate();
    }

    public static <O> Predicate<O> In(Collection<O> value) {
        return new Predicates.InPredicate<>(value);
    }

    public static <O> Predicate<O> NotIn(Collection<O> value) {
        return In(value).negate();
    }

    public static Predicate<String> StringContains(String value) {
        return new Predicates.StringContainsPredicate(value);
    }

    public static Predicate<String> NotStringContains(String value) {
        return StringContains(value).negate();
    }

    public static Predicate<String> StringMatches(String value) {
        return new Predicates.StringMatchesPredicate(value);
    }

    public static Predicate<String> NotStringMatches(String value) {
        return StringMatches(value).negate();
    }


    public static <O extends Comparable<O>> Predicate<O> After(O min) {
        return new Predicates.AfterPredicate<>(min, false);
    }

    public static <O extends Comparable<O>> Predicate<O> AfterOrEquals(O min) {
        return new Predicates.AfterPredicate<>(min, true);
    }

    public static <O extends Comparable<O>> Predicate<O> Before(O max) {
        return new Predicates.BeforePredicate<>(max, false);
    }

    public static <O extends Comparable<O>> Predicate<O> BeforeOrEquals(O max) {
        return new Predicates.BeforePredicate<>(max, true);
    }

    public static <O extends Comparable<O>> Predicate<O> Between(O min, O max) {
        return new Predicates.AfterPredicate<>(min, false).and(new Predicates.BeforePredicate<>(max, false));
    }

    public static <O extends Comparable<O>> Predicate<O> BetweenOrEquals(O min, O max) {
        return new Predicates.AfterPredicate<>(min, true).and(new Predicates.BeforePredicate<>(max, true));
    }

    public static final class EqualsPredicate<O> implements Predicate<O> {
        private final O value;

        public EqualsPredicate(O value) {
            this.value = Objects.requireNonNull(value);
        }

        @Override
        public boolean test(O o) {
            return Objects.equals(o, value);
        }
    }

    public static final class InPredicate<O> implements Predicate<O> {
        private final Collection<O> value;

        public InPredicate(Collection<O> value) {
            this.value = Objects.requireNonNull(value);
        }

        @Override
        public boolean test(O o) {
            return value.contains(o);
        }
    }

    public static final class BeforePredicate<O extends Comparable<O>> implements Predicate<O> {
        private final O value;
        private final boolean orEquals;

        public BeforePredicate(O value, boolean orEquals) {
            this.value = Objects.requireNonNull(value);
            this.orEquals = orEquals;
        }

        @Override
        public boolean test(O o) {
            int i = o.compareTo(value);
            return i < 0 || i == 0 && orEquals;
        }

    }

    public static final class AfterPredicate<O extends Comparable<O>> implements Predicate<O> {
        private final O value;
        private final boolean orEquals;

        public AfterPredicate(O value, boolean orEquals) {
            this.value = Objects.requireNonNull(value);
            this.orEquals = orEquals;
        }

        @Override
        public boolean test(O o) {
            int i = o.compareTo(value);
            return i > 0 || i == 0 && orEquals;
        }

    }

    public static final class StringContainsPredicate implements Predicate<String> {
        private final String value;

        public StringContainsPredicate(String value) {
            this.value = Objects.requireNonNull(value);
        }

        @Override
        public boolean test(String o) {
            return o.contains(value);
        }

    }

    public static final class StringMatchesPredicate implements Predicate<String> {
        private final String value;

        public StringMatchesPredicate(String value) {
            this.value = Objects.requireNonNull(value);
        }

        @Override
        public boolean test(String o) {
            return o.matches(value);
        }
    }
}
