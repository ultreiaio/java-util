package io.ultreia.java4all.util;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Replace old nuiton-version (org.nuiton.version.Version).
 * <p>
 * Created on 13/11/2021.
 *
 * <p>A class to represent a version.</p>
 * <p>Replace previously {@code org.nuiton.util.Version} class.</p>
 * <h2>Definition</h2>
 * A version is defined of n {@code components} separated by {@code componentSeparator}.
 * <h3>Components</h3>
 * Components can be of two types:
 *  <ul>
 *  <li>Number component: a strict positive integer value</li>
 *  <li>String component: a sequence of characters which can't be either number nor component separators</li>
 *  </ul>
 *  <h3>Component separators</h3>
 *  <p>
 *  Component separator are characters which can't be alphanumeric and can be {@code empty character}.
 *  </p>
 *  <p>
 *  Component separators are optional and components will be detected as soon as a character changed from a numeric string sequence to a alpha (none numeric!) sequence.
 *  </p>
 *  <p>
 *  For example, version {@code 1a2} is composed of three components: {code 1}, {@code a} and {@code 3}.
 *  </p>
 *  <h3>Snapshot flag</h3>
 *  Additionally version can be qualified as a {@code SNAPSHOT} (see below section about ordering).
 *  <h2>Examples</h2>
 *  <pre>
 *   0 (one component 0)
 *   0-SNAPSHOT (one component 0 + SNAPSHOT flag)
 *   1.0 (two components 1,0)
 *   1.1 (two components 1,1)
 *   1.1-alpha-1 (four components 1,1,alpha,1)
 *   1.1-beta (three components 1,1,beta)
 *   1.1-rc-1 (four components 1,1,rc,1)
 *   1.1-a  (three components 1,1,a)
 *   1.1-a12-4.45_6432 (seven components 1,1,a,12,4,45,643)
 *  </pre>
 * <h2>Ordering</h2>
 * A version is comparable, to have all the detail of order see {@link VersionComparator}.
 * <h2>Immutability</h2>
 * The version is immutable, to create or modify a version, use the {@link VersionBuilder} API
 * or shortcut methods.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.14
 */
@SuppressWarnings({"unused"})
public class Version implements Comparable<Version>, Serializable {

    /**
     * Default Pre release classifiers
     */
    private static final List<String> PRE_RELEASE_CLASSIFIERS = List.of("alpha","beta","rc","m");
    /**
     * Suffix of a {@code SNAPSHOT} version in the text representation.
     */
    public static final String SNAPSHOT_SUFFIX = "-SNAPSHOT";
    /**
     * Version V0.
     */
    public static final Version VZERO = create().build();
    /**
     * Default component separator.
     */
    public static final char DEFAULT_JOIN_COMPONENT_SEPARATOR = '.';
    /**
     * Comparator of version used internally to fulfill the comparator contract.
     */
    protected static final VersionComparator VERSION_COMPARATOR = new VersionComparator();
    private static final long serialVersionUID = 1L;
    /**
     * List of components of the version.
     */
    protected final List<VersionComponent<?, ?>> components;

    /**
     * List of separators of the version.
     */
    protected final List<String> componentSeparators;

    /**
     * flag to define if version is a snapshot (if so a -SNAPSHOT is
     * added at the end of the textual representation of the version).
     */
    protected final boolean snapshot;

    /**
     * string representation of the version.
     */
    protected transient String version;

    public static class NumberVersionComponent implements VersionComponent<Integer, NumberVersionComponent> {

        private static final long serialVersionUID = 1L;

        protected final Integer value;

        public NumberVersionComponent(Integer value) {
            this.value = value;
        }

        @Override
        public Integer getValue() {
            return value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof NumberVersionComponent)) return false;
            return compareTo((NumberVersionComponent) o) == 0;
        }

        @Override
        public int hashCode() {
            return value.hashCode();
        }

        @Override
        public int compareTo(NumberVersionComponent o) {
            return value.compareTo(o.value);
        }
    }

    public static class StringVersionComponent implements VersionComponent<String, StringVersionComponent> {

        private static final long serialVersionUID = 1L;

        protected final boolean preRelease;

        protected final String value;

        protected final String lowerCaseValue;

        public StringVersionComponent(boolean preRelease, String value) {
            this.preRelease = preRelease;
            this.value = value;
            this.lowerCaseValue = value.toLowerCase();
        }

        @Override
        public String getValue() {
            return value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof StringVersionComponent)) return false;
            return compareTo((StringVersionComponent) o) == 0;
        }

        @Override
        public int hashCode() {
            int result = preRelease ? 1 : 0;
            result = 31 * result + value.hashCode();
            return result;
        }

        public boolean isPreRelease() {
            return preRelease;
        }

        @Override
        public int compareTo(StringVersionComponent o) {
            int result;
            if (!Objects.equals(preRelease, o.preRelease)) {
                // compare on preRelease flag
                if (preRelease) {
                    // preRelease is before any other string component
                    result = -1;
                } else {
                    result = 1;
                }
            } else {
                // on same preRelease flag
                if (preRelease) {
                    int i0 = PRE_RELEASE_CLASSIFIERS.indexOf(getValue().toLowerCase());
                    int i1 = PRE_RELEASE_CLASSIFIERS.indexOf(o.getValue().toLowerCase());
                    result = i0 - i1;
                } else {
                    // compare on lowerCaseValue
                    result = lowerCaseValue.compareTo(o.lowerCaseValue);
                }
            }
            return result;
        }
    }

    @SuppressWarnings("UnusedReturnValue")
    public static class VersionBuilder {

        /**
         * Classifiers known as pre-release classifiers.
         */
        protected Set<String> preReleaseClassifiers;
        /**
         * Set of characters used to split components.
         */
        protected Set<Character> splitSeparators;
        /**
         * The character to join component in a string representation.
         */
        protected Character joinSeparator;
        /**
         * The string representation of the version.
         */
        protected String version;
        /**
         * List of components of the version.
         */
        protected List<Comparable<?>> components;
        /**
         * List of components separators explicitly defined (used as {@link Version#componentSeparators}.
         */
        protected List<String> componentSeparators;
        /**
         * Is the version a snapshot ? In a such case, the {@link #version} ends with {@code -SNAPSHOT}.
         */
        protected boolean snapshot;

        public VersionBuilder() {
            splitSeparators = new HashSet<>();
            splitSeparators.add('-');
            splitSeparators.add('.');
            splitSeparators.add('_');
            joinSeparator = '.';
            preReleaseClassifiers = new HashSet<>();
            preReleaseClassifiers.addAll(PRE_RELEASE_CLASSIFIERS);
        }

        public VersionBuilder setVersion(String version) {
            checkState(!fromComponents(), "Can't set version if components are filled");
            this.version = version;
            return this;
        }

        public VersionBuilder setComponents(List<Comparable<?>> components) {
            checkState(!fromStringRepresentation(), "Can't set components if version is filled");
            this.components = components;
            return this;
        }

        public VersionBuilder setComponent(int level, Comparable<?> value) {
            checkState(!fromStringRepresentation(), "Can't set components if version is filled");
            int size = components.size();
            checkArgument(level >= 0 && level < size, "level should be in [0, " + (size - 1) + "]");
            components.set(level, value);
            return this;
        }

        public VersionBuilder addComponent(Comparable<?> value) {
            checkState(!fromStringRepresentation(), "Can't set components if version is filled");
            components.add(value);
            return this;
        }

        public VersionBuilder addComponent(Comparable<?> value, char componentSeparator) {

            addComponent(value);
            if (componentSeparators == null) {
                // init it with join separator
                componentSeparators = new ArrayList<>(components.size() - 1);
                initSeparatorList(componentSeparators, components.size() - 2);
            }
            componentSeparators.add(componentSeparator + "");
            return this;
        }

        public VersionBuilder removeComponent(int level) {
            checkState(!fromStringRepresentation(), "Can't set components if version is filled");
            int size = components.size();
            checkArgument(level >= 0 && level < size, "level should be in [0, " + (size - 1) + "]");
            this.components.remove(level);
            if (componentSeparators != null) {
                // remove this separator
                this.componentSeparators.remove(level - 1);
            }
            return this;
        }

        public VersionBuilder setComponentSeparators(List<String> componentSeparators) {
            checkState(!fromStringRepresentation(), "Can't set components if version is filled");
            checkArgument(componentSeparators.size() == components.size() - 1, "Must have n-1 component separators if you have n components");
            this.componentSeparators = componentSeparators;
            return this;
        }

        public VersionBuilder setComponentSeparator(int level, char value) {
            checkState(!fromStringRepresentation(), "Can't set components if version is filled");
            int size = components.size();
            checkArgument(level >= 0 && level < size, "level should be in [0, " + (size - 2) + "]");
            this.componentSeparators.set(level, value + "");
            return this;
        }

        public VersionBuilder setSnapshot(boolean snapshot) {
            checkState(!fromStringRepresentation(), "Can't set snapshot if version is filled");
            this.snapshot = snapshot;
            return this;
        }

        public VersionBuilder setSplitSeparators(Set<Character> splitSeparators) {
            for (Character splitSeparator : splitSeparators) {
                checkState(!isAsciiAlphanumeric(splitSeparator), "Can't use a alphanumeric splitSeparator");
            }
            this.splitSeparators = splitSeparators;
            return this;
        }

        public VersionBuilder setJoinSeparator(Character joinSeparator) {
            this.joinSeparator = joinSeparator;
            return this;
        }

        public VersionBuilder setPreReleaseClassifiers(Set<String> preReleaseClassifiers) {
            this.preReleaseClassifiers = preReleaseClassifiers;
            return this;
        }

        protected boolean fromStringRepresentation() {
            return version != null;
        }

        protected boolean fromComponents() {
            return components != null;
        }

        public Version build() {
            List<VersionComponent<?, ?>> finalComponents = new ArrayList<>();
            List<String> finalComponentSeparators = new ArrayList<>();
            boolean finalSnapshot;
            if (fromComponents()) {
                finalSnapshot = snapshot;
                for (Comparable<?> component : components) {
                    boolean number = component instanceof Integer;
                    VersionComponent<?, ?> v = toVersionComponent(number, String.valueOf(component));
                    finalComponents.add(v);
                }
                if (componentSeparators != null && !componentSeparators.isEmpty()) {
                    finalComponentSeparators = new ArrayList<>(componentSeparators);
                } else {
                    initSeparatorList(finalComponentSeparators, finalComponents.size() - 1);
                }
            } else if (fromStringRepresentation()) {
                // compute components + snapshot from version
                finalSnapshot = this.version.endsWith(Version.SNAPSHOT_SUFFIX);
                String versionPart = substringBeforeLast(this.version, Version.SNAPSHOT_SUFFIX);
                // --- split by splitSeparator --- //
                boolean numberState = false;
                String currentComponentStr = null;
                for (int i = 0, l = versionPart.length(); i < l; i++) {
                    char currentChar = versionPart.charAt(i);
                    if (splitSeparators.contains(currentChar)) {
                        // end of a component
                        finalComponentSeparators.add(currentChar + "");
                        if (currentComponentStr != null) {
                            // register new component
                            VersionComponent<?, ?> component = toVersionComponent(numberState, currentComponentStr);
                            finalComponents.add(component);
                            currentComponentStr = null;
                        }
                    } else {
                        boolean number = isAsciiNumeric(currentChar);
                        if (currentComponentStr == null) {
                            // start a new component
                            currentComponentStr = "" + currentChar;
                            numberState = number;
                        } else if (numberState == number) {
                            // still on same type
                            // concat to current component
                            currentComponentStr += currentChar;
                        } else {
                            // new component detected
                            // finalize the current component
                            VersionComponent<?, ?> component = toVersionComponent(numberState, currentComponentStr);
                            finalComponents.add(component);
                            finalComponentSeparators.add("");
                            // start the new component
                            numberState = number;
                            currentComponentStr = "" + currentChar;
                        }
                    }
                }
                if (currentComponentStr != null) {
                    // flush last component
                    VersionComponent<?, ?> component = toVersionComponent(numberState, currentComponentStr);
                    finalComponents.add(component);
                }
            } else {
                // empty version
                finalSnapshot = snapshot;
                finalComponents.add(new NumberVersionComponent(0));
            }
            if (finalComponentSeparators.isEmpty() && finalComponents.size() > 1) {
                // use component join separator
                initSeparatorList(finalComponentSeparators, finalComponents.size() - 1);
            }
            return new Version(finalComponents, finalComponentSeparators, finalSnapshot);
        }

        protected void initSeparatorList(List<String> list, int size) {
            for (int i = 0; i < size; i++) {
                list.add("" + joinSeparator);
            }
        }

        protected VersionComponent<?, ?> toVersionComponent(boolean numberState, String currentComponentStr) {
            VersionComponent<?, ?> component;
            if (numberState) {
                // was a number
                component = new NumberVersionComponent(Integer.valueOf(currentComponentStr));
            } else {
                // was a string
                boolean preRelease = preReleaseClassifiers.contains(currentComponentStr.toLowerCase());
                component = new StringVersionComponent(preRelease, currentComponentStr);
            }
            return component;
        }
    }

    public static class VersionComparator implements Comparator<Version>, Serializable {

        private static final long serialVersionUID = 1L;

        @Override
        public int compare(Version o1, Version o2) {
            int o1NbComponents = o1.getComponentCount();
            int o2NbComponents = o2.getComponentCount();
            int minComponentSize = Math.min(o1NbComponents, o2NbComponents);
            int maxComponentSize = Math.max(o1NbComponents, o2NbComponents);
            int result = 0;
            for (int i = 0; result == 0 && i < minComponentSize; i++) {
                @SuppressWarnings("rawtypes") VersionComponent o1Component = o1.getComponent(i);
                @SuppressWarnings("rawtypes") VersionComponent o2Component = o2.getComponent(i);
                if (Objects.equals(o1Component.getClass(), o2Component.getClass())) {
                    // same component type, using natural order
                    //noinspection unchecked
                    result = o1Component.compareTo(o2Component);
                } else {
                    // different component type
                    // classifier is always lower than number component
                    if (o1Component instanceof NumberVersionComponent) {
                        result = 1;
                    } else {
                        result = -1;
                    }
                }
            }
            if (result == 0 && minComponentSize != maxComponentSize) {
                // same value base on common components
                // check type of next component on the longest version
                if (o2NbComponents == minComponentSize) {
                    // o1 has more components
                    VersionComponent<?, ?> component = o1.getComponent(minComponentSize);
                    if (component instanceof StringVersionComponent) {
                        // o1 has a string
                        StringVersionComponent stringVersionComponent = (StringVersionComponent) component;
                        if (stringVersionComponent.isPreRelease()) {
                            // o1 is pre-release so before o2
                            result = -1;
                        } else {
                            // o1 is post release so after o2
                            result = 1;
                        }
                    } else {
                        // o1 has one more number component: after o2
                        result = 1;
                    }
                } else {
                    // o2 has more components
                    VersionComponent<?, ?> component = o2.getComponent(minComponentSize);
                    if (component instanceof StringVersionComponent) {
                        // o2 has a string
                        StringVersionComponent stringVersionComponent = (StringVersionComponent) component;
                        if (stringVersionComponent.isPreRelease()) {
                            // o2 is pre-release so before o1
                            result = 1;
                        } else {
                            // o2 is post release so after o1
                            result = -1;
                        }
                    } else {
                        // o2 has one more number component: after o1
                        result = -1;
                    }
                }
            }
            if (result == 0 && !Objects.equals(o1.isSnapshot(), o2.isSnapshot())) {
                // snapshot is lower than none snapshot
                if (o2.isSnapshot()) {
                    // o2 is snapshot
                    result = 1;
                } else {
                    // o1 is snapshot
                    result = -1;
                }
            }
            return result;
        }
    }

    /**
     * Shortcut method to get a version from his string representation.
     *
     * @param version string representation of the version
     * @return converted version from the string representation
     */
    public static Version valueOf(String version) {
        return create(version).build();
    }

    public static VersionBuilder create() {
        return new VersionBuilder();
    }

    public static VersionBuilder create(String version) {
        return new VersionBuilder().setVersion(version);
    }

    public static VersionBuilder create(Version version) {
        List<Comparable<?>> components = toComparableList(version.getComponents());
        List<String> componentSeparators = new ArrayList<>(version.getComponentSeparators());
        boolean snapshot = version.isSnapshot();
        return create(components).setComponentSeparators(componentSeparators).setSnapshot(snapshot);
    }

    public static VersionBuilder create(List<Comparable<?>> components) {
        return new VersionBuilder().setComponents(components);
    }

    /**
     * Create a version from the given one and set to it the {@code snapshot} state to {@code true}.
     *
     * @param version version to clone
     * @return the cloned version with the {@code snapshot} state to {@code true}
     * @throws IllegalArgumentException if {@code snapshot} state is already set to {@code true} on
     *                                  the given {@code version}.
     */
    public static Version addSnapshot(Version version) {
        if (version.isSnapshot()) {
            throw new IllegalArgumentException("version " + version + "is already a snapshot");
        }
        return create(version).setSnapshot(true).build();
    }

    /**
     * Create a version from the given one and set to it the {@code snapshot} state to {@code false}.
     *
     * @param version version to clone
     * @return the cloned version with the {@code snapshot} state to {@code true}
     * @throws IllegalArgumentException if {@code snapshot} state is already set to {@code false} on
     *                                  the given {@code version}
     */
    public static Version removeSnapshot(Version version) {
        if (!version.isSnapshot()) {
            throw new IllegalArgumentException("version " + version + "is already a snapshot");
        }
        return create(version).setSnapshot(false).build();
    }

    /**
     * Create a new version containing a single component from a given version.
     *
     * @param version   original version
     * @param component component index to extract
     * @return new {@link Version} with a single component
     */
    public static Version extractVersion(Version version, int component) {
        return extractVersion(version, component, component);
    }

    /**
     * Create a new version containing a sub set of component from a given version.
     *
     * @param version        original version
     * @param firstComponent first component index
     * @param lastComponent  last component index
     * @return new {@link Version} with a components sub set
     */
    public static Version extractVersion(Version version, int firstComponent, int lastComponent) {
        if (lastComponent < firstComponent) {
            throw new IllegalArgumentException("lastComponent must be greater or equals to firstComponent");
        }

        // extract components
        List<Comparable<?>> components = new ArrayList<>();
        for (int index = firstComponent; index <= lastComponent; index++) {
            Comparable<?> component = version.getComponent(index).getValue();
            components.add(component);
        }

        return create().setComponents(components).build();
    }

    /**
     * Creates a new version from this one incremented.
     * <p>
     * If the last component is a number, then just increments this number; otherwise add a new
     * number component with value 1.
     * <p>
     * Example:
     * <ul>
     * <li>1 → 2</li>
     * <li>1-a → 1-a.1</li>
     * </ul>
     *
     * @param version version to increment
     * @return the incremented version
     */
    public static Version increments(Version version) {
        return increments(version, Version.DEFAULT_JOIN_COMPONENT_SEPARATOR);
    }

    /**
     * Creates a new version from this one incremented.
     * <p>
     * If the last component is a number, then just increments this number; otherwise add a new
     * number component with value 1.
     * <p>
     * Example:
     * <ul>
     * <li>1 → 2</li>
     * <li>1-a → 1-a.1</li>
     * </ul>
     *
     * @param version            version to increment
     * @param componentSeparator the component separator to use the last component is a classifier
     * @return the incremented version
     */
    public static Version increments(Version version, char componentSeparator) {
        Version newVersion;
        VersionComponent<?, ?> lastComponent = version.getLastComponent();
        if (lastComponent instanceof StringVersionComponent) {
            // must then add new number component with value 1
            newVersion = create(version).addComponent(1, componentSeparator).build();
        } else {
            // increments it
            int numberComponent = ((NumberVersionComponent) lastComponent).getValue();
            newVersion = create(version).setComponent(version.getComponentCount() - 1, numberComponent + 1).build();
        }
        return newVersion;
    }

    /**
     * Creates a new version from this one with the number component incremented at the given position.
     * <p>
     * <strong>Note:</strong>
     * Will fail if the component at the required position is not a number.
     *
     * @param version           version to increment
     * @param componentPosition position of the version component to increment
     * @return the incremented version
     */
    public static Version increments(Version version, int componentPosition) {
        int numberComponent = version.getNumberComponent(componentPosition);
        return create(version).setComponent(componentPosition, numberComponent + 1).build();
    }

    /**
     * Tests if two versions are equals.
     *
     * @param version0 the first version
     * @param version1 the second version
     * @return {@code true} if versions are equals, {@code false} otherwise.
     */
    public static boolean equals(String version0, String version1) {
        Version v0 = valueOf(version0);
        Version v1 = valueOf(version1);
        return v0.equals(v1);
    }

    /**
     * Creates a new version from this one with the number component decremented at the given position.
     * <p>
     * <strong>Note:</strong>
     * Will fail if the component at the required position is not a number, or his value is 0.
     *
     * @param version           version to decrement
     * @param componentPosition position of the version component to increment
     * @return the decremented version
     */
    public static Version decrements(Version version, int componentPosition) {
        int numberComponent = version.getNumberComponent(componentPosition);
        checkArgument(componentPosition > 0, "Component at position " + componentPosition + " values 0, can't decrement it.");
        return create(version).setComponent(componentPosition, numberComponent - 1).build();
    }

    /**
     * Tests if the first version is smaller than the second version.
     *
     * @param version0 the first version
     * @param version1 the second version
     * @return {@code true} if {@code version0} is before {@code version1},
     * {@code false} otherwise.
     */
    public static boolean smallerThan(String version0, String version1) {
        Version v0 = valueOf(version0);
        Version v1 = valueOf(version1);
        return v0.before(v1);
    }

    /**
     * Tests if the first version is greater than the second version.
     *
     * @param version0 the first version
     * @param version1 the second version
     * @return {@code true} if {@code version0} is after {@code version1},
     * {@code false} otherwise.
     */
    public static boolean greaterThan(String version0, String version1) {
        Version v0 = valueOf(version0);
        Version v1 = valueOf(version1);
        return v0.after(v1);
    }

    /**
     * <p>Gets the substring before the last occurrence of a separator.
     * The separator is not returned.</p>
     *
     * <p>A {@code null} string input will return {@code null}.
     * An empty ("") string input will return the empty string.
     * An empty or {@code null} separator will return the input string.</p>
     *
     * <p>If nothing is found, the string input is returned.</p>
     *
     * <pre>
     * Version.substringBeforeLast(null, *)      = null
     * Version.substringBeforeLast("", *)        = ""
     * Version.substringBeforeLast("ab-cd", "b") = "ab-c"
     * Version.substringBeforeLast("abc", "c")   = "ab"
     * Version.substringBeforeLast("a", "a")     = ""
     * Version.substringBeforeLast("a", "z")     = "a"
     * Version.substringBeforeLast("a", null)    = "a"
     * Version.substringBeforeLast("a", "")      = "a"
     * </pre>
     *
     * @param str       the String to get a substring from, may be null
     * @param separator the String to search for, may be null
     * @return the substring before the last occurrence of the separator,
     * {@code null} if null String input
     * @since 2.0
     */
    private static String substringBeforeLast(final String str, final String separator) {
        if (str.isEmpty() || separator.isEmpty()) {
            return str;
        }
        final int pos = str.lastIndexOf(separator);
        if (pos == -1) {
            return str;
        }
        return str.substring(0, pos);
    }

    private static void checkState(boolean condition, String message) {
        if (!condition) {
            throw new IllegalStateException(message);
        }
    }

    private static void checkArgument(boolean condition, String message) {
        if (!condition) {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * <p>Checks whether the character is ASCII 7 bit numeric.</p>
     *
     * <pre>
     *   CharUtils.isAsciiAlphanumeric('a')  = true
     *   CharUtils.isAsciiAlphanumeric('A')  = true
     *   CharUtils.isAsciiAlphanumeric('3')  = true
     *   CharUtils.isAsciiAlphanumeric('-')  = false
     *   CharUtils.isAsciiAlphanumeric('\n') = false
     *   CharUtils.isAsciiAlphanumeric('&copy;') = false
     * </pre>
     *
     * @param ch the character to check
     * @return true if between 48 and 57 or 65 and 90 or 97 and 122 inclusive
     */
    private static boolean isAsciiAlphanumeric(char ch) {
        return (ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z') || (ch >= '0' && ch <= '9');
    }

    /**
     * <p>Checks whether the character is ASCII 7 bit numeric.</p>
     *
     * <pre>
     *   CharUtils.isAsciiNumeric('a')  = false
     *   CharUtils.isAsciiNumeric('A')  = false
     *   CharUtils.isAsciiNumeric('3')  = true
     *   CharUtils.isAsciiNumeric('-')  = false
     *   CharUtils.isAsciiNumeric('\n') = false
     *   CharUtils.isAsciiNumeric('&copy;') = false
     * </pre>
     *
     * @param ch the character to check
     * @return true if between 48 and 57 inclusive
     */
    private static boolean isAsciiNumeric(char ch) {
        return ch >= '0' && ch <= '9';
    }

    private static List<Comparable<?>> toComparableList(List<VersionComponent<?, ?>> components) {
        return components.stream().map(VersionComponent::getValue).collect(Collectors.toList());
    }

    protected Version(List<VersionComponent<?, ?>> components, List<String> componentSeparators, boolean snapshot) {
        this.componentSeparators = List.copyOf(componentSeparators);
        this.components = List.copyOf(components);
        this.snapshot = snapshot;
    }

    public List<VersionComponent<?, ?>> getComponents() {
        return components;
    }

    public List<String> getComponentSeparators() {
        return componentSeparators;
    }

    public boolean isSnapshot() {
        return snapshot;
    }

    public int getComponentCount() {
        return components.size();
    }

    public int getNumberComponent(int componentPosition) {
        VersionComponent<?, ?> comparable = getComponent(componentPosition);
        checkState(comparable instanceof NumberVersionComponent, "component at " + componentPosition + " for version " + this + " is not a number (" + comparable + ")");
        return (Integer) comparable.getValue();
    }

    public String getTextComponent(int componentPosition) {
        VersionComponent<?, ?> comparable = getComponent(componentPosition);
        checkState(comparable instanceof StringVersionComponent, "component at " + componentPosition + " for version " + this + " is not a string (" + comparable + ")");
        return (String) comparable.getValue();
    }

    public VersionComponent<?, ?> getComponent(int level) {
        checkArgument(level > 0 || level < getComponentCount(), "not a valid level " + level + " for the Version " + this);
        return components.get(level);
    }

    /**
     * @return the string representation value of the version
     */
    public String getVersion() {
        if (version == null) {
            version = String.valueOf(components.get(0).getValue());
            for (int i = 0, nb = componentSeparators.size(); i < nb; i++) {
                version += componentSeparators.get(i);
                version += components.get(i + 1).getValue();
            }
            if (snapshot) {
                version += SNAPSHOT_SUFFIX;
            }
        }
        return version;
    }

    /**
     * Convert the string representation to a java identifier compliant.
     *
     * <ul>
     * <li>in java: {@code .} is forbidden</li>
     * <li>in database (mysql, h2 ...): {@code .} is forbidden</li>
     * </ul>
     * <p>
     * Forbidden values are replaced by {@code _} character.
     *
     * @return the java compliant string representation of the version
     */
    public String getValidName() {
        String validName = getVersion();
        // replace ". et -"
        validName = validName.replaceAll("[.\\-]", "_");
        return validName;
    }

    @Override
    public int compareTo(Version o) {
        return VERSION_COMPARATOR.compare(this, o);
    }

    /**
     * @param o the other version to test
     * @return {@code true} if current version is before or equals the given one
     */
    public boolean beforeOrEquals(Version o) {
        int result = compareTo(o);
        return result <= 0;
    }

    /**
     * @param o the other version to test
     * @return {@code true} if current version is before the given one
     */
    public boolean before(Version o) {
        int result = compareTo(o);
        return result < 0;
    }

    /**
     * @param o the other version to test
     * @return {@code true} if current version is after or equals the given one
     */
    public boolean afterOrEquals(Version o) {
        int result = compareTo(o);
        return result >= 0;
    }

    /**
     * @param o the other version to test
     * @return {@code true} if current version is after the given one
     */
    public boolean after(Version o) {
        int result = compareTo(o);
        return result > 0;
    }

    @Override
    public String toString() {
        return getVersion();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Version)) return false;
        return compareTo((Version) o) == 0;
    }

    @Override
    public int hashCode() {
        int result = components.hashCode();
        result = 31 * result + (snapshot ? 1 : 0);
        return result;
    }

    protected VersionComponent<?, ?> getLastComponent() {
        return components.get(getComponentCount() - 1);
    }

    public interface VersionComponent<C extends Comparable<C>, V extends VersionComponent<C, V>> extends Serializable, Comparable<V> {
        C getValue();
    }

}
