package io.ultreia.java4all.util.io;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

/**
 * Created on 10/12/2021.
 *
 * @param <O> type of object
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.16
 */
public interface ReaderContract<O> extends ConfigFormat {
    /**
     * Read from given {@code path}.
     *
     * @param path location of object to read
     * @return loaded object
     * @throws ReaderException if error while reading
     */
    default O read(Path path) throws ReaderException {
        try {
            return read(path.toUri().toURL(), StandardCharsets.UTF_8);
        } catch (MalformedURLException e) {
            throw new ReaderException(String.format("[%s] Can't read file: %s", getFormat(), path), e);
        }
    }

    /**
     * Read from given {@code url}.
     *
     * @param url      url where to read
     * @param encoding encoding used to read storage
     * @return loaded object
     * @throws ReaderException if any read error
     */
    default O read(URL url, Charset encoding) throws ReaderException {
        try (Reader reader = new BufferedReader(new InputStreamReader(url.openStream(), encoding))) {
            return read(reader, url.toString().substring(url.toString().lastIndexOf("/") + 1));
        } catch (Exception e) {
            throw new ReaderException(String.format("[%s] Can't read file: %s", getFormat(), url), e);
        }
    }

    /**
     * Read from given reader.
     *
     * @param reader   where to read
     * @param fileName file name of config
     * @return loaded object
     * @throws Exception if any error while reading
     */
    O read(Reader reader, String fileName) throws Exception;
}
