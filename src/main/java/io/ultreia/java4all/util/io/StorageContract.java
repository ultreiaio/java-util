package io.ultreia.java4all.util.io;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.SingletonSupplier;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.ServiceLoader;

/**
 * Created on 10/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.16
 * @param <O>    type of object
 */
public interface StorageContract<O> extends ReaderContract<O>, WriterContract<O> {

    /**
     * Get object for the given {@code format}.
     *
     * @param format format of object to get
     * @param cache  cache of objects
     * @param <O>    type of object
     * @return the found object
     * @throws IllegalArgumentException if no object found for the given format
     * @throws NullPointerException     if format is null
     */
    static <O> O get(Map<String, O> cache, String format) {
        O storage = cache.get(Objects.requireNonNull(format));
        if (storage == null) {
            throw new IllegalArgumentException(String.format("Can't find storage for format: %s, available formats: %s", format, cache.keySet()));
        }
        return storage;
    }

    static <O, C extends StorageContract<O>> SingletonSupplier<Map<String, C>> load(Class<C> type) {
        return load(type.getClassLoader(), type);
    }

    static <O, C extends StorageContract<O>> SingletonSupplier<Map<String, C>> load(ClassLoader classLoader, Class<C> type) {
        return SingletonSupplier.of(() -> {
            Map<String, C> result = new LinkedHashMap<>();
            for (C o : ServiceLoader.load(type, classLoader)) {
                String format = o.getFormat();
                C previous = result.put(format, o);
                if (previous != null) {
                    throw new IllegalStateException(String.format("Not unique storage for format %s (type: %s)", format, type.getName()));
                }
            }
            return Collections.unmodifiableMap(result);
        });
    }
}
