package io.ultreia.java4all.util.io;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.BufferedWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created on 10/12/2021.
 *
 * @param <O> type of object
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.16
 */
public interface WriterContract<O> extends ConfigFormat {

    /**
     * Write model to the given {@code path}.
     *
     * @param model    model to store
     * @param comment  optional comment
     * @param path     location of config to write
     * @param encoding encoding of writer
     * @throws WriterException if error while reading config
     */
    default void write(O model, String comment, Path path, Charset encoding) throws WriterException {
        try (BufferedWriter writer = Files.newBufferedWriter(path, encoding)) {
            write(model, comment, writer);
        } catch (Exception e) {
            throw new WriterException(String.format("[%s] Can't write file: %s", getFormat(), path), e);
        }
    }

    /**
     * Write model into the given {@code writer}.
     *
     * @param model   model to store
     * @param comment optional comment to write
     * @param writer  where to write
     * @throws Exception if any error while writing
     */
    void write(O model, String comment, BufferedWriter writer) throws Exception;
}
