package io.ultreia.java4all.util.json;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.function.BiConsumer;

/**
 * Provides json adapter offered by the services loader mechanism.
 * <p>
 * Created on 12/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see JsonAdapter
 * @see JsonHierarchicAdapter
 * @since 1.1.0
 */
public class JsonAdapterProvider {

    private static final Logger log = LogManager.getLogger(JsonAdapterProvider.class);

    private static JsonAdapterProvider INSTANCE;
    private final Map<Class<?>, JsonAdapter> adapters;
    private final Map<Class<?>, JsonHierarchicAdapter> hierarchicAdapters;

    public static JsonAdapterProvider get() {
        return INSTANCE == null ? INSTANCE = new JsonAdapterProvider() : INSTANCE;
    }

    public static void offers(BiConsumer<Class<?>, JsonAdapter> adapterConsumer,
                              BiConsumer<Class<?>, JsonHierarchicAdapter> hierarchicAdapterConsumer) {
        JsonAdapterProvider provider = get();
        provider.adapters.forEach(adapterConsumer);
        provider.hierarchicAdapters.forEach(hierarchicAdapterConsumer);
    }

    private JsonAdapterProvider() {
        Map<Class<?>, JsonAdapter> adapters = new LinkedHashMap<>();
        Map<Class<?>, JsonHierarchicAdapter> hierarchicAdapters = new LinkedHashMap<>();
        for (JsonAdapter adapter : ServiceLoader.load(JsonAdapter.class)) {
            Class<?> type = adapter.type();
            log.info(String.format("Found json type: %s (%s)", type.getName(), adapter));
            adapters.put(type, adapter);
        }
        for (JsonHierarchicAdapter adapter : ServiceLoader.load(JsonHierarchicAdapter.class)) {
            Class<?> type = adapter.type();
            log.info(String.format("Found hierarchic json type: %s (%s)", type.getName(), adapter));
            hierarchicAdapters.put(type, adapter);
        }
        this.adapters = Collections.unmodifiableMap(adapters);
        this.hierarchicAdapters = Collections.unmodifiableMap(hierarchicAdapters);
    }
}
