package io.ultreia.java4all.util.json;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Register it with services loaders mechanism.
 * <p>
 * {@link JsonAdapterProvider} will add it to json builder as a simple adapter for the given {@link #type()}.
 * <p>
 * Created on 12/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.0
 */
public interface JsonAdapter {

    Class<?> type();
}
