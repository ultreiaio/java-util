package io.ultreia.java4all.util.json.adapters;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;
import io.ultreia.java4all.util.json.JsonAdapter;
import io.ultreia.java4all.util.sql.BlobsContainer;
import io.ultreia.java4all.util.sql.SqlScript;

import java.lang.reflect.Type;
import java.util.Set;

/**
 * Created by tchemit on 17/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.0
 */
@AutoService(JsonAdapter.class)
public class SqlScriptAdapter implements JsonDeserializer<SqlScript>, JsonSerializer<SqlScript>, JsonAdapter {

    public static final String LOCATION = "location";
    public static final String BLOBS = "blobs";
    private static final Type TYPE = TypeToken.getParameterized(Set.class, BlobsContainer.class).getType();

    @Override
    public Class<?> type() {
        return SqlScript.class;
    }

    @Override
    public SqlScript deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        byte[] location = context.deserialize(jsonObject.get(LOCATION), byte[].class);
        SqlScript result = SqlScript.of(location);
        JsonElement blobsElement = jsonObject.get(BLOBS);
        if (blobsElement != null) {
            Set<BlobsContainer> blobs = context.deserialize(blobsElement, TYPE);
            result.addBlobsContainers(blobs);
        }
        return result;
    }

    @Override
    public JsonElement serialize(SqlScript src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject element = new JsonObject();
        element.add(LOCATION, context.serialize(src.toByteArray()));
        if (src.withBlobs()) {
            element.add(BLOBS, context.serialize(src.getBlobsContainers()));
        }
        return element;
    }
}

