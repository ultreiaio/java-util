package io.ultreia.java4all.util.json;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.reflect.TypeToken;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 01/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.0
 */
public class JsonHelper {

    public static String readStringOrNull(String name, JsonObject object) {
        JsonPrimitive element = object.getAsJsonPrimitive(name);
        return element != null ? element.getAsString() : null;
    }

    public static String readString(String name, JsonObject object) {
        JsonPrimitive element = object.getAsJsonPrimitive(name);
        return element != null ? element.getAsString() : "";
    }

    public static List<String> readStringList(JsonDeserializationContext context, String name, JsonObject object) {
        JsonElement element = object.get(name);
        return element == null ? null : List.of(context.deserialize(element, String[].class));
    }

    public static Set<String> readStringSetOrEmpty(JsonDeserializationContext context, String name, JsonObject object) {
        JsonElement element = object.get(name);
        return element == null ? Set.of() : new LinkedHashSet<>(List.of(context.deserialize(element, String[].class)));
    }

    public static boolean readBoolean(String name, JsonObject object) {
        JsonPrimitive element = object.getAsJsonPrimitive(name);
        return element != null && element.getAsBoolean();
    }

    public static <O> O readObject(JsonDeserializationContext context, String name, Class<O> type, JsonObject object) {
        JsonElement element = object.get(name);
        return element == null ? null : context.deserialize(element, type);
    }

    public static <O> List<O> readObjectListOrEmpty(JsonDeserializationContext context, String name, Class<O> type, JsonObject object) {
        JsonElement element = object.get(name);
        return element == null ? List.of() : context.deserialize(element, TypeToken.getParameterized(List.class, type).getType());
    }

    public static <O> List<O> readObjectList(JsonDeserializationContext context, String name, Class<O> type, JsonObject object) {
        JsonElement element = object.get(name);
        return element == null ? null : context.deserialize(element, TypeToken.getParameterized(List.class, type).getType());
    }

    public static <K, V> Map<K, V> readObjectMapOrEmpty(JsonDeserializationContext context, String name, Class<K> keyType, Class<V> valueType, JsonObject object) {
        JsonElement element = object.get(name);
        return element == null ? Map.of() : context.deserialize(element, TypeToken.getParameterized(Map.class, keyType, valueType).getType());
    }

    public static <K, V> Map<K, V> readObjectMap(JsonDeserializationContext context, String name, Class<K> keyType, Class<V> valueType, JsonObject object) {
        JsonElement element = object.get(name);
        return element == null ? null : context.deserialize(element, TypeToken.getParameterized(Map.class, keyType, valueType).getType());
    }

    public static void addToResult(JsonSerializationContext context, JsonObject result, String name, Object table) {
        if (table == null) {
            return;
        }
        if (table instanceof Boolean && (!(Boolean) table)) {
            return;
        }
        if (table instanceof String && ((String) table).isEmpty()) {
            return;
        }
        if (table instanceof Collection && ((Collection<?>) table).isEmpty()) {
            return;
        }
        if (table instanceof Map && ((Map<?, ?>) table).isEmpty()) {
            return;
        }
        result.add(name, context.serialize(table));
    }

    public static void addToProperties(List<Object> properties, Object table) {
        if (table == null) {
            return;
        }
        if (table instanceof Boolean && (!(Boolean) table)) {
            properties.add(null);
            return;
        }
        if (table instanceof String && ((String) table).isEmpty()) {
            properties.add(null);
            return;
        }
        if (table instanceof Collection && ((Collection<?>) table).isEmpty()) {
            properties.add(null);
            return;
        }
        if (table instanceof Map && ((Map<?, ?>) table).isEmpty()) {
            properties.add(null);
            return;
        }
        properties.add(table.toString());
    }

    public static String codeProperties(List<Object> properties) {
        return properties.stream().map(o -> o == null ? "" : o.toString()).collect(Collectors.joining("~"));
    }
}
