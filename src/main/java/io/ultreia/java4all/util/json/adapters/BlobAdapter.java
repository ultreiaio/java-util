package io.ultreia.java4all.util.json.adapters;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import io.ultreia.java4all.util.json.JsonAdapter;

import javax.sql.rowset.serial.SerialBlob;
import java.lang.reflect.Type;
import java.sql.Blob;
import java.sql.SQLException;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.0
 */
@AutoService(JsonAdapter.class)
public class BlobAdapter implements JsonSerializer<Blob>, JsonDeserializer<Blob>, JsonAdapter {
    @Override
    public Class<?> type() {
        return Blob.class;
    }


    @Override
    public Blob deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        byte[] bytes = context.deserialize(json, byte[].class);
        try {
            return new SerialBlob(bytes);
        } catch (SQLException e) {
            throw new JsonParseException("could not create blob ", e);
        }
    }

    @Override
    public JsonElement serialize(Blob src, Type typeOfSrc, JsonSerializationContext context) {
        try {
            byte[] bytes = src.getBytes(1, (int) src.length());
            return context.serialize(bytes);
        } catch (SQLException e) {
            throw new JsonParseException("could not read blob ", e);
        }
    }
}
