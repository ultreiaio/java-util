package io.ultreia.java4all.util;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Optional;

/**
 * Created by tchemit on 20/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class Comparators {

    /**
     * Compute a optional comparator of given {@code comparators} (if collection is empty, no comparator is produce then).
     *
     * @param comparators comparators to concat
     * @param <O>         type of object of comparators
     * @return optional concatenated comparator
     */
    public static <O> Optional<Comparator<O>> comparator(Collection<Comparator<O>> comparators) {
        Iterator<Comparator<O>> iterator = comparators.iterator();
        if (!iterator.hasNext()) {
            return Optional.empty();
        }
        Comparator<O> result = iterator.next();
        while (iterator.hasNext()) {
            result = result.thenComparing(iterator.next());
        }
        return Optional.of(result);
    }
}
