package io.ultreia.java4all.util;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Helper to produce imports list.
 * <p>
 * Used for example in processors or generators.
 * <p>
 * Created by tchemit on 20/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ImportManager {

    private final String defaultPackage;

    private final Set<String> importSet = new LinkedHashSet<>();

    public ImportManager(String defaultPackage) {
        this.defaultPackage = defaultPackage;
    }

    /**
     * Register a type and return his simple name
     *
     * @param type type to register
     * @return the simple name of given type
     */
    public String addImport(Class<?> type) {
        if (!Objects.requireNonNull(type).isPrimitive()
                && !"java.lang".equals(type.getPackage().getName())
                && (defaultPackage == null || !defaultPackage.equals(type.getPackage().getName()))) {
            importSet.add(type.getCanonicalName());
        }
        return type.getSimpleName();
    }

    /**
     * Register a type and return his simple name
     *
     * @param type type to register
     * @return the simple name of given type
     */
    public String addImport(String type) {
        int lastIndexOf = Objects.requireNonNull(type).lastIndexOf(".");
        if (lastIndexOf == -1) {
            return type;
        }
        String typePackage = type.substring(0, lastIndexOf);
        if (!typePackage.equals(defaultPackage)) {
            importSet.add(type);
        }
        return type.substring(typePackage.length() + 1);
    }

    /**
     * @return sorted list of imports
     */
    public List<String> getImportsList() {
        List<String> importList = new LinkedList<>(importSet);
        importList.sort(String::compareTo);
        return importList;
    }

    /**
     * Generate the import section of a java file.
     *
     * @param eol end of line separator
     * @return import section
     */
    public String getImportsSection(String eol) {
        StringBuilder importsBuilder = new StringBuilder();
        for (String importType : getImportsList()) {
            importsBuilder.append("import ").append(importType).append(";").append(eol);
        }
        return importsBuilder.toString();
    }

}
