package io.ultreia.java4all.util.matrix;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.json.JsonAware;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.util.Set;
import java.util.StringJoiner;
import java.util.function.Function;

/**
 * Data matrix.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public final class DataMatrix implements JsonAware {

    private static final Logger log = LogManager.getLogger(DataMatrix.class);
    private static final Function<Object, String> DEFAULT_CELL_VALUE_FORMATTER = o -> o == null ? null : o.toString();

    private Object[][] data;

    private int width;

    private int height;

    private int x;

    private int y;
    /**
     * Used to format a cell value to store
     * <p>
     * See {@link #setValue(Object)} and {@link #setValue(int, int, Object)}.
     * <p>
     * Since 1.1.19
     */
    private transient final Function<Object, String> cellValueFormatter;

    public DataMatrix() {
        this(null);
    }

    public DataMatrix(Function<Object, String> cellValueFormatter) {
        this.cellValueFormatter = cellValueFormatter == null ? DEFAULT_CELL_VALUE_FORMATTER : cellValueFormatter;
    }

    public static DataMatrixDimension getDimension(DataMatrix... matrices) {
        int width = 0;
        int height = 0;

        for (DataMatrix request : matrices) {

            int nWidth = request.getX() + request.getWidth();
            int nHeight = request.getY() + request.getHeight();
            if (nWidth > width) {
                width = nWidth;
            }
            if (nHeight > height) {
                height = nHeight;
            }
        }
        return new DataMatrixDimension(width, height);
    }

    public static DataMatrix create(int positionX, int positionY, int width, int height) {
        DataMatrix result = new DataMatrix();
        result.setLocation(new DataMatrixPoint(positionX, positionY));
        result.setDimension(new DataMatrixDimension(width, height));
        result.createData();
        return result;
    }

    public static DataMatrix merge(DataMatrix... matrices) {
        return merge(-1, -1, matrices);
    }

    public static DataMatrix merge(int rows,
                                   int columns,
                                   DataMatrix... matrices) {
        DataMatrixDimension dimension = getDimension(matrices);

        log.debug(String.format("Merge dimension : %s", dimension));

        if (rows != -1) {
            int height = dimension.getHeight();
            // on verifie que récupère bien le bon count de lignes
            if (rows != height) {
                log.warn(String.format("No matching rows number : should have %d, but was %d", rows, height));
            }
        }

        if (columns != -1) {
            int width = dimension.getWidth();
            // on verifie que récupère bien le bon count de colonnes
            if (columns != width) {
                log.warn(String.format("No matching columns number : should have %d, but was %d", columns, width));
            }
        }

        DataMatrix result = new DataMatrix();
        result.setDimension(dimension);
        result.createData();
        for (DataMatrix incoming : matrices) {
            result.copyData(incoming);
        }
        return result;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Object[][] getData() {
        return data;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void createData() {
        data = new Object[height][width];
    }

    public void setData(Object[][] data) {
        this.data = data;
    }

    public void copyData(DataMatrix incoming) {

        int x = incoming.getX();
        int y = incoming.getY();

        int height = incoming.getHeight();
        int width = incoming.getWidth();

        log.debug(String.format("copying incoming matrix (dim: %s, location: %s)", incoming.getDimension(), incoming.getLocation()));

        for (int i = 0; i < width; i++) {
            if (x + i < 0) {
                continue;
            }
            for (int j = 0; j < height; j++) {
                if (y + j < 0) {
                    continue;
                }
                Serializable value = incoming.getValue(i, j);
                setValue(x + i, y + j, value);
            }
        }
    }

    public void simpleCopyRow(DataMatrix incoming, int with, int targetJ, int incomingJ) {
        for (int i = 0; i < with; i++) {
            Serializable value = incoming.getValue(i, incomingJ);
            setValue(i, targetJ, value);
        }
    }

    public void copyRow(DataMatrix incoming, int width, int targetJ, int incomingX, int incomingJ, Set<Integer> keys, String[] result) {
        int index = 0;
        for (int i = 0; i < width; i++) {
            Serializable value = incoming.getValue(i + incomingX, incomingJ);
            setValue(i, targetJ, value);
            if (keys.contains(i)) {
                result[index++] = (String) value;
            }
        }
    }

    public DataMatrixDimension getDimension() {
        return new DataMatrixDimension(width, height);
    }

    public void setDimension(DataMatrixDimension dim) {
        height = dim.getHeight();
        width = dim.getWidth();
    }

    public DataMatrixPoint getLocation() {
        return new DataMatrixPoint(x, y);
    }

    public void setLocation(DataMatrixPoint location) {
        x = location.getX();
        y = location.getY();
    }

    public Serializable getValue(int x, int y) {
        return data == null ? null : (Serializable) data[y][x];
    }

    public void setValue(int x, int y, Object data) {
        String cellData = formatCellValue(data);
        log.debug(String.format("Put data [%d,%d] = %s", x, y, cellData));
        this.data[y][x] = cellData;
    }

    public DataMatrix setValue(Object data) {
        String cellData = formatCellValue(data);
        for (int y = 0; y < this.y; y++) {
            for (int x = 0; x < this.x; x++) {
                this.data[y][x] = cellData;
            }
        }
        return this;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", DataMatrix.class.getSimpleName() + "[", "]")
                .add("dimension=" + getDimension())
                .add("location=" + getLocation())
                .toString();
    }

    public Function<Object, String> getCellValueFormatter() {
        return cellValueFormatter;
    }

    public String getClipboardContent(boolean copyRowHeaders,
                                      boolean copyColumnHeaders,
                                      boolean escapeCells,
                                      char sep) {
        if (getWidth() <= 0 || getHeight() <= 0) {
            return "";
        }
        StringBuilder buffer = new StringBuilder();
        char eol = '\n';
        for (int y = copyColumnHeaders ? 0 : 1, rows = getHeight(); y < rows; y++) {
            Serializable value;
            // new line
            int x = copyRowHeaders ? 0 : 1;
            for (int columns = getWidth() - 1; x < columns; x++) {
                // all cells except the last one
                value = getValue(x, y);
                if (escapeCells) {
                    value = escapeCell(value);
                }
                buffer.append(value).append(sep);
            }
            // last cell
            value = getValue(x, y);
            if (escapeCells) {
                value = escapeCell(value);
            }
            buffer.append(value);
            // end of line
            buffer.append(eol);
        }
        return buffer.toString();
    }

    private String escapeCell(Object value) {
        return String.format("\"%s\"", value == null ? "" : value.toString().replaceAll("\"", "\"\""));
    }

    private String formatCellValue(Object data) {
        return getCellValueFormatter().apply(data);
    }
}
