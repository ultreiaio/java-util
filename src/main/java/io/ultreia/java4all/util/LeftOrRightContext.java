package io.ultreia.java4all.util;

/*-
 * #%L
 * Java Util extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2025 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Represents a specialized {@link TwoSideContext} for {@link LeftOrRight} type of side.
 * <p>
 * Created at 22/04/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.16
 */
public interface LeftOrRightContext<O> extends TwoSideContext<LeftOrRight, O> {

    static <O> LeftOrRightContext<O> of(O left, O right) {
        return new LeftOrRightContextImpl<>(left, right);
    }

    default <V> LeftOrRightContext<V> apply(Function<O, V> function) {
        return of(function.apply(left()), function.apply(right()));
    }

    default <V, X> LeftOrRightContext<V> apply(LeftOrRightContext<X> otherContext, BiFunction<X, O, V> function) {
        return of(function.apply(otherContext.left(), left()), function.apply(otherContext.right(), right()));
    }

    default LeftOrRightContext<O> flip() {
        return of(right(), left());
    }

    class LeftOrRightContextImpl<O> extends TwoSideContextImpl<LeftOrRight, O> implements LeftOrRightContext<O> {

        public LeftOrRightContextImpl(O left, O right) {
            super(left, right);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof LeftOrRightContext)) return false;
            LeftOrRightContext<?> that = (LeftOrRightContext<?>) o;
            return Objects.equals(left(), that.left()) && Objects.equals(right(), that.right());
        }
    }
}
